export const SET_PASSDATA = "teamly/settings/SET_PASSDATA";

const initialState = {
    messageToPassUrl: ""
};

export default (state = initialState, action) => {
    switch (action.type) {
        case SET_PASSDATA:
            const { messageToPassUrl } = action;
            return {
                ...state,
                messageToPassUrl
            };
        default:
            return state;
    }
};

export const setPassData = (
    messageToPassUrl
) => ({
    type: SET_PASSDATA,
    messageToPassUrl
});
