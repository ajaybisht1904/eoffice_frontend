import * as AT from '../../constants/ActionTypes'
import {BACK_API} from '../../../../middleware/backend';
import {BACK_API1} from '../../../../middleware/backendPA';
import {BACKEND_API_MISS} from "../../../../middleware/backendMIS";
import localStorageService from "../../../../services/localStorageService";

export const getClassificationData = () => ({
    [BACK_API]: {
        types: [ AT.CLASSIFICATION_REQUEST, AT.CLASSIFICATION_SUCCESS, AT.CLASSIFICATION_FAILURE ],
        endpoint: `/api/getFileClassification`,
        settings: {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Authorization':'Bearer '+sessionStorage .getItem('jwt_token')
            }
        }
    }

});

export const getTypeData = () => ({
    [BACK_API]: {
        types: [ AT.TYPE_REQUEST, AT.TYPE_SUCCESS, AT.TYPE_FAILURE ],
        endpoint: `/api/getFileType`,
        settings: {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Authorization':'Bearer '+sessionStorage .getItem('jwt_token')
            }
        }
    }

});

export const getRolesData= () => ({
    [BACK_API]: {
        types: [ AT.ROLES_REQUEST, AT.ROLES_SUCCESS, AT.ROLES_FAILURE ],
        endpoint: `/api/getRoles`,
        settings: {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Authorization':'Bearer '+sessionStorage .getItem('jwt_token')
            }
        }
    }

});

export const getUserRolesData= (department) => ({
    [BACK_API]: {
        types: [ AT.USER_ROLES_REQUEST, AT.USER_ROLES_SUCCESS, AT.USER_ROLES_FAILURE ],
        endpoint: `/api/getUserRoles`,
        settings: {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Authorization':'Bearer '+sessionStorage .getItem('jwt_token'),
                'groupName': department
            }
        }
    }

});

// export const getUserRolesData= (department) => ({
//     [BACKEND_API_MISS]: {
//         types: [ AT.USER_ROLES_REQUEST, AT.USER_ROLES_SUCCESS, AT.USER_ROLES_FAILURE ],
//         endpoint: `/apis/getUserRoles`,
//         settings: {
//             method: 'GET',
//             headers: {
//                 'Accept': 'application/json',
//                 'Authorization':'Bearer '+sessionStorage .getItem('jwt_token'),
//                 'groupName': department
//             }
//         }
//     }

// });

export const getGroupsData = () => ({
    [BACK_API]: {
        types: [ AT.GROUPS_REQUEST, AT.GROUPS_SUCCESS, AT.GROUPS_FAILURE ],
        endpoint: `/api/getGroups`,
        settings: {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Authorization':'Bearer '+sessionStorage .getItem('jwt_token')
            }
        }
    }

});

export const getDraftData = (role) => ({
    [BACK_API]: {
        types: [ AT.DRAFT_DATA_FAILURE, AT.DRAFT_DATA_SUCCESS, AT.DRAFT_DATA_REQUEST ],
        endpoint: `/api/getDraftData`,
        settings: {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Authorization':'Bearer '+sessionStorage .getItem('jwt_token'),
                'roleName': role
            }
        }
    }

});

export const getOutboxData = (role,username,pageSize, pageNumber) => ({
    [BACK_API]: {
        types: [ AT.OUTBOX_DATA_FAILURE, AT.OUTBOX_DATA_REQUEST, AT.OUTBOX_DATA_SUCCESS ],
        endpoint: `/api/getOutboxData`,
        settings: {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Authorization':'Bearer '+sessionStorage .getItem('jwt_token'),
                'roleName': role,
                'userName': username,
                'pageSize': pageSize,
                'pageNumber': pageNumber
            }
        }
    }

});

export const getInboxData = (role,username,pageSize,pageNumber) => ({
    [BACK_API]: {
        types: [ AT.INBOX_DATA_FAILURE, AT.INBOX_DATA_REQUEST, AT.INBOX_DATA_SUCCESS ],
        endpoint: `/api/getInboxData`,
        settings: {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Authorization':'Bearer '+sessionStorage .getItem('jwt_token'),
                'roleName': role,
                'userName': username,
                'pageSize': pageSize,
                'pageNumber': pageNumber
            }
        }
    }

});

export const getEnclosureData = (id) => ({
    [BACK_API]: {
        types: [ AT.ENCLOSURE_DATA_FAILURE, AT.ENCLOSURE_DATA_REQUEST, AT.ENCLOSURE_DATA_SUCCESS ],
        endpoint: `/api/enclosure/data/`+id,
        settings: {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Authorization':'Bearer '+sessionStorage .getItem('jwt_token')
            }
        }
    }

});


export const getNotingData = (id) => ({
    [BACK_API]: {
        types: [ AT.NOTING_DATA_FAILURE, AT.NOTING_DATA_REQUEST, AT.NOTING_DATA_SUCCESS ],
        endpoint: `/api/noting/data/`+id,
        settings: {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Authorization':'Bearer '+sessionStorage .getItem('jwt_token')
            }
        }
    }

});

export const getFileUrl = (url) => ({
    [BACK_API]: {
        types: [ AT.FILE_FAILURE, AT.FILE_REQUEST, AT.FILE_SUCCESS ],
        endpoint: `/api/fileUrl`,
        settings: {
            method: 'GET',
            headers: {
                'Authorization':'Bearer '+sessionStorage .getItem('jwt_token'),
                'url' : url
            }
        }
    }

});

export const getSfdt = (url,username,id,role,grp) => ({
    [BACK_API]: {
        types: [ AT.SFDT_FAILURE, AT.SFDT_REQUEST, AT.SFDT_SUCCESS ],
        endpoint: `/api/sfdt`,
        settings: {
            method: 'GET',
            headers: {
                'Authorization':'Bearer '+sessionStorage .getItem('jwt_token'),
                'url' : url,
                'userName': username,
                'fileId':id,
                'roleName': role,
                'groupName':grp
            }
        }
    }

});

export const getFileTypeData = () => ({
    [BACK_API]: {
        types: [ AT.PATYPE_REQUEST, AT.PATYPE_SUCCESS, AT.PATYPE_FAILURE ],
        endpoint: `/api/getPersonalFileType`,
        settings: {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Authorization':'Bearer '+sessionStorage .getItem('jwt_token')
            }
        }
    }

});

export const getPF = (username,role,pageSize, pageNumber) => ({
    [BACK_API]: {
        types: [ AT.PF_REQUEST, AT.PF_SUCCESS, AT.PF_FAILURE ],
        endpoint: `/api/getPersonalFile`,
        settings: {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Authorization':'Bearer '+sessionStorage .getItem('jwt_token'),
                'userName': username,
                'roleName': role,
                'pageSize': pageSize,
                'pageNumber': pageNumber
            }
        }
    }

});

export const getPFileData = (username,role) => ({
    [BACK_API]: {
        types: [ AT.PFILE_REQUEST, AT.PFILE_SUCCESS, AT.PFILE_FAILURE ],
        endpoint: `/api/getPFile`,
        settings: {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Authorization':'Bearer '+sessionStorage .getItem('jwt_token'),
                'userName': username,
                'roleName': role
            }
        }
    }

});

export const getPATableData = (username,role,pageSize, pageNumber) => ({
    [BACK_API]: {
        types: [ AT.PA_TABLE_REQUEST, AT.PA_TABLE_SUCCESS, AT.PA_TABLE_FAILURE ],
        endpoint: `/api/getPersonalApplicationData`,
        settings: {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Authorization':'Bearer '+sessionStorage .getItem('jwt_token'),
                'userName': username,
                'roleName': role,
                'pageSize': pageSize,
                'pageNumber': pageNumber
            }
        }
    }

});

export const getAnnexureTableData = (id) => ({
    [BACK_API]: {
        types: [ AT.ANNEXURE_LIST_REQUEST, AT.ANNEXURE_LIST_SUCCESS, AT.ANNEXURE_LIST_FAILURE ],
        endpoint: `/api/getAnnexureData/`+id,
        settings: {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Authorization':'Bearer '+sessionStorage .getItem('jwt_token')
            }
        }
    }

});

// export const getMISTableData = (value) => ({
//     [BACKEND_API_MISS]: {
//         types: [ AT.MIS_LIST_REQUEST, AT.MIS_LIST_SUCCESS, AT.MIS_LIST_FAILURE ],
//         endpoint: `/api/gethierarchyParent1List`,
//         settings: {
//             method: 'POST',
//             body: value,
//             headers: { }
//         }
//     }
//
// });

export const getMISTableData = (value) => ({
    [BACKEND_API_MISS]: {
        types: [ AT.MIS_LIST_REQUEST, AT.MIS_LIST_SUCCESS, AT.MIS_LIST_FAILURE ],
        endpoint: `/api/getSauData`,
        settings: {
            method: 'GET',
            headers: {
                groupName: value
            }
        }
    }
});
export const getMISDetailTableData = (value) => ({
    [BACKEND_API_MISS]: {
        types: [ AT.MIS_DETAIL_LIST_REQUEST, AT.MIS_DETAIL_LIST_SUCCESS, AT.MIS_DETAIL_LIST_FAILURE ],
        endpoint: `/api/gettotalfiles`,
        settings: {
            method: 'POST',
            body: value,
            headers: { }
        }
    }

});

export  const deleteAnnexureData = (id) => ({
    [BACK_API]: {
        types: [ AT.DELETE_ANNEXURE_REQUEST, AT.DELETE_ANNEXURE_SUCCESS, AT.DELETE_ANNEXURE_FAILURE ],
            endpoint: `/api/deleteAnnexureData/`+id,
            settings: {
            method: 'POST',
        }
    }
});

export  const createPANotingData = (data,role,userName,groupName) => ({
    [BACK_API]: {
        types: [ AT.CREATE_PANOTING_REQUEST, AT.CREATE_PANOTING_SUCCESS, AT.CREATE_PANOTING_FAILURE ],
        endpoint: `/api/createPartCaseFile`,
        settings: {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Authorization':'Bearer '+sessionStorage .getItem('jwt_token'),
                'roleName': role,
                'username': userName,
                'grp': groupName
            },
            body: data
        }
    }
});

export  const getPANotingData = (id) => ({
    [BACK_API]: {
        types: [ AT.FETCH_PANOTING_REQUEST, AT.FETCH_PANOTING_SUCCESS, AT.FETCH_PANOTING_FAILURE ],
        endpoint: `/api/getPANotingData/`+id,
        settings: {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Authorization':'Bearer '+sessionStorage .getItem('jwt_token')
            }
        }
    }
});

export  const getPAEnclosureData = (ids,id,role,groupName) => ({
    [BACK_API]: {
        types: [ AT.FETCH_PAENCLOSURE_REQUEST, AT.FETCH_PAENCLOSURE_SUCCESS, AT.FETCH_PAENCLOSURE_FAILURE ],
        endpoint: `/api/getPAEnclosureData/`+id,
        settings: {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Authorization':'Bearer '+sessionStorage .getItem('jwt_token'),
                'roleName': role,
                'inboxId': ids,
                'grp': groupName
            }
        }
    }
});

export const getPADraftTableData = (username,role,pageSize, pageNumber) => ({
    [BACK_API]: {
        types: [ AT.PADRAFT_REQUEST, AT.PADRAFT_SUCCESS, AT.PADRAFT_FAILURE ],
        endpoint: `/api/getDraftData`,
        settings: {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Authorization':'Bearer '+sessionStorage .getItem('jwt_token'),
                'userName': username,
                'roleName': role,
                'pageSize': pageSize, 
                'pageNumber': pageNumber
            }
        }
    }

});