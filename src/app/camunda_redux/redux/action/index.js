import * as ProcessDefinitionActions from './camunda-rest/process-definition';
import * as TaskActions from './camunda-rest/task';
import * as InitiateActions from './backend-rest/initiate-data';
import * as FormDataAction from './backend-rest/form-data';

export const loadTasks = () => (dispatch, getState) => {
  return dispatch(TaskActions.fetchTasks())
};

export const getPersonalInfo = (values) => (dispatch, getState) => {
    return dispatch(FormDataAction.getPersonalInfo(values));
};

export const updatePersonalInfo = (values) => (dispatch, getState) => {
    return dispatch(FormDataAction.updatePersonalInfo(values));
};

export const loadTaskFormKey = (taskId) => (dispatch, getState) => {
  return dispatch(TaskActions.fetchTaskFormKey(taskId))
};

export const completeTask = (taskId, values) => (dispatch, getState) => {
  return dispatch(TaskActions.postCompleTask(taskId, values))
};

export const loadProcessDefinitions = (processDefinitionId) => (dispatch, getState) => {
  return dispatch(ProcessDefinitionActions.fetchProcessDefinitions(processDefinitionId))
};

export const loadProcessDefinitionsWithXML = (processDefinitionId) => (dispatch, getState) => {
  return dispatch(ProcessDefinitionActions.fetchProcessDefinitions(processDefinitionId)).then((data) => {
    data.response.result.forEach((id) => {
      dispatch(ProcessDefinitionActions.fetchProcessDefinitionXML(id))
    });

  })
};

export const loadProcessDefinitionXML = (processDefinitionId) => (dispatch, getState) => {
  return dispatch(ProcessDefinitionActions.fetchProcessDefinitionXML(processDefinitionId))
};

export const loadFormKey = (processDefinitionKey) => (dispatch, getState) => {
  return dispatch(ProcessDefinitionActions.fetchFormKey(processDefinitionKey))
};

export const startProcessInstance = (processDefinitionKey, values) => (dispatch, getState) => {
  return dispatch(ProcessDefinitionActions.postProcessInstance(processDefinitionKey, values))
};

export const loadTaskVariables = (taskId, variableNames) => (dispatch, getState) => {
  return dispatch(TaskActions.fetchTaskVariables(taskId, variableNames))
};

export const loadClassificationData = () => (dispatch, getState) => {
  return dispatch(InitiateActions.getClassificationData())
};

export const loadTypesData = () => (dispatch, getState) => {
  return dispatch(InitiateActions.getTypeData())
};
export const loadFileTypesData = (role) => (dispatch, getState) => {
  return dispatch(InitiateActions.getFileTypeData(role))
};

export const loadGroupsData = () => (dispatch, getState) => {
  return dispatch(InitiateActions.getGroupsData())
};

export const loadRolesData = () => (dispatch, getState) => {
  return dispatch(InitiateActions.getRolesData())
};

export const loadDraftData = (role) => (dispatch, getState) => {
    return dispatch(InitiateActions.getDraftData(role))
};

export const loadOutboxData = (role,username,pageSize, pageNumber) => (dispatch, getState) => {
    return dispatch(InitiateActions.getOutboxData(role,username,pageSize, pageNumber))
};

export const loadInboxData = (role,username,pageSize,pageNumber) => (dispatch, getState) => {
    return dispatch(InitiateActions.getInboxData(role,username,pageSize,pageNumber))
};

export const loadEnclosureData = (id) => (dispatch, getState) => {
    return dispatch(InitiateActions.getEnclosureData(id))
};

export const loadNotingData = (id) => (dispatch, getState) => {
    return dispatch(InitiateActions.getNotingData(id))
};

export const downloadFile = (url) => (dispatch, getState) => {
    return dispatch(InitiateActions.getFileUrl(url))
};

export const createFormData = (values) => (dispatch, getState) => {
  return dispatch(FormDataAction.setCreateForm(values))
};

export const uploadEnclosure = (id,file,role,username) => (dispatch, getState) => {
    return dispatch(FormDataAction.uploadEnclosure(id,file,role,username))
};

export const uploadNoting = (id,file,role,username) => (dispatch, getState) => {
    return dispatch(FormDataAction.uploadNoting(id,file,role,username))
};

export const sendFile = (id,data,role) => (dispatch, getState) => {
    return dispatch(FormDataAction.sendFile(id,data,role))
};

export const loadInstanceVariables = (id) => (dispatch, getState) => {
    return dispatch(ProcessDefinitionActions.postProcessInstanceVariables(id))
};

export const loadSfdt = (url,username,id,role,dept) => (dispatch, getState) => {
    return dispatch(InitiateActions.getSfdt(url,username,id,role,dept))
};
export const personalFileFormData = (values) => (dispatch, getState) => {
    return dispatch(FormDataAction.createPersonalFileForm(values))
};
export const personalApplicationFormData = (values,role,grp) => (dispatch, getState) => {
    return dispatch(FormDataAction.createPersonalApplicationForm(values,role,grp))
};
export const loadPFData = (username,role, pageSize, pageNumber) => (dispatch, getState) => {
    return dispatch(InitiateActions.getPF(username,role, pageSize, pageNumber))
};

export const loadPFileData = (username,role) => (dispatch, getState) => {
    return dispatch(InitiateActions.getPFileData(username,role))
};
export const loadPATableData = (username,role,pageSize, pageNumber) => (dispatch, getState) => {
    return dispatch(InitiateActions.getPATableData(username,role,pageSize, pageNumber))
};

export const quickSign = (value,role) => (dispatch, getState) => {
    return dispatch(FormDataAction.quickSign(value,role));
};

export const sendFiles = (id,data,role,username) => (dispatch, getState) => {
    return dispatch(FormDataAction.sendFiles(id,data,role,username))
};

export const sendFilesSection = (id,data,role,username) => (dispatch, getState) => {
    return dispatch(FormDataAction.sendFilesSection(id,data,role,username))
};

export const sendFilesServiceNumber = (id,data,role,username) => (dispatch, getState) => {
    return dispatch(FormDataAction.sendFilesServiceNumber(id,data,role,username))
};

export const saveDocument = (id,formData,role, isPartCase) => (dispatch, getState) => {
    return dispatch(FormDataAction.saveFiles(id,formData,role, isPartCase))
};

export const saveAnnotation = (values,id,flag) => (dispatch, getState) => {
    return dispatch(FormDataAction.createAnnotation(values,id,flag))
};

export const getAnnotation = (id) => (dispatch, getState) => {
    return dispatch(FormDataAction.getAnnotation(id))
};

export const uploadAnnexure = (personalAppId,file,role,username,onUploadProgress) => (dispatch, getState) => {
    return dispatch(FormDataAction.uploadAnnexure(personalAppId,file,role,username,onUploadProgress))
};

export const getGroupList = (value) => (dispatch, getState) => {
    return dispatch(FormDataAction.getGroupList(value))
};

export const getHrmListData = (value) => (dispatch, getState) => {
    return dispatch(FormDataAction.getHrmFileList(value))
};

export const loadAnnexureTableData = (id) => (dispatch, getState) => {
    return dispatch(InitiateActions.getAnnexureTableData(id))
};

export const loadUserRoleData = (department) => (dispatch, getState) => {
    return dispatch(InitiateActions.getUserRolesData(department))
};

export const getMISTableList = (value) => (dispatch, getState) => {
    return dispatch(InitiateActions.getMISTableData(value))
};

export const getMISDetailTableList = (value) => (dispatch, getState) => {
    return dispatch(InitiateActions.getMISDetailTableData(value))
};

export const deleteAnnexureData = (id) => (dispatch, getState) => {
    return dispatch(InitiateActions.deleteAnnexureData(id))
};

export const createPANotingData = (data,role,userName,groupName) => (dispatch, getState) => {
    return dispatch(InitiateActions.createPANotingData(data,role,userName,groupName))
};

export const getPANotingData = (id) => (dispatch, getState) => {
    return dispatch(InitiateActions.getPANotingData(id))
};

export const getPAEnclosureData = (ids,id,role,groupName) => (dispatch, getState) => {
    return dispatch(InitiateActions.getPAEnclosureData(ids,id,role,groupName))
};
export const getbyfilename = (values) => (dispatch, getState) => {
    return dispatch(FormDataAction.getbyfilename(values))
};
export const getSection = () => (dispatch, getState) => {
    return dispatch(FormDataAction.getSection())
};
export const getServiceNumber = () => (dispatch, getState) => {
    return dispatch(FormDataAction.getServiceNumber())
};
export const loadPADraftTableData = (username,role,pageSize, pageNumber) => (dispatch, getState) => {
    return dispatch(InitiateActions.getPADraftTableData(username,role,pageSize, pageNumber))
};
export const loadPartCaseData = (data) => (dispatch, getState) => {
    return dispatch(FormDataAction.getPartCaseData(data))
};

export const loadInboxDataSplitView = (id) => (dispatch, getState) => {
    return dispatch(FormDataAction.getSplitViewInboxData(id))
};