import { applyMiddleware, createStore } from 'redux'
import thunk from 'redux-thunk'
import api from './../../middleware/api'
import backend from './../../middleware/backend'
import rootReducer from './reducer/index'
import backendPA from '../../middleware/backendPA'
import backendMIS from '../../middleware/backendMIS'
import backendSau from '../../middleware/backendSau'
import backend_axios from '../../middleware/backend_axios'
import backend_annexure from '../../middleware/backend_annexure'
import backend_annotation from 'app/middleware/backend_annotation'
import backend_createPA from 'app/middleware/backend_createPA'
import backend_createPF from 'app/middleware/backend_createPF'
import backend_hrm from 'app/middleware/backend_hrm'
import backend_personalinfo from 'app/middleware/backend_personalinfo'
import backend_retrieval from 'app/middleware/backend_retrieval'
import backend_send from 'app/middleware/backend_send'
import backend_sign from 'app/middleware/backend_sign'
import backend_userManagement from 'app/middleware/backend_userManagement'
import backend_util from 'app/middleware/backend_util'


const configureStore = preloadedState => createStore(
    rootReducer,
    preloadedState,
    applyMiddleware(thunk,
        api,
        backend,
        backendPA,
        backendMIS,
        backendSau,
        backend_axios,
        backend_annexure,
        backend_annotation,
        backend_createPA,
        backend_createPF,
        backend_hrm,
        backend_personalinfo,
        backend_retrieval,
        backend_send,
        backend_sign,
        backend_userManagement,
        backend_util)
)

export default configureStore;
