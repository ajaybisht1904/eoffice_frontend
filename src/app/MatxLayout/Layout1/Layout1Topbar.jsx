import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import {
    FormControl,
    Icon,
    IconButton,
    MenuItem,
    withStyles,
    MuiThemeProvider, Select, Tooltip,
} from "@material-ui/core";
import { connect } from "react-redux";
import { setLayoutSettings } from "app/redux/actions/LayoutActions";
import { logoutUser } from "app/redux/actions/UserActions";
import { loadUserRoleData } from '../../camunda_redux/redux/action'
import { PropTypes } from "prop-types";
import { MatxMenu } from "./../../../matx";
import { isMdScreen } from "utils";
import NotificationBar from "../SharedCompoents/NotificationBar";
import { changingTableStatePA, changingTableState, changingTableStateInbox, changingTableStateOutbox } from "../../camunda_redux/redux/action/apiTriggers";
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import "./Layout1.css";

const styles = theme => ({
    root: {
        backgroundColor: theme.palette.primary.main
    }
});

const elem = document.documentElement;

class Layout1Topbar extends Component {
    state = {
        fullScreen: false,
        comboValue: "",
        comboList: [],
        lightMode: false
    };

    // componentWillMount() {
    //     const department = sessionStorage.getItem("department");
    //     this.props.loadUserRoleData(department).then(resp => {
    //         let tempArr = [];
    //         try{
    //         for (let x = 0; x < resp.data.length; x++) {
    //             tempArr.push(resp.data[x])
    //         }
    //         if (tempArr.length > 0) {
    //             this.setState({ comboList: tempArr, comboValue: tempArr[0] });
    //             sessionStorage.setItem("role", this.state.comboValue);
    //         }
    //     }
    //     catch(e){
    //         if(e.message === "Cannot read property 'roleName' of undefined"){
    //             this.props.history.push("/costa/404")
    //         }
    //     }
    //     })
    // }

    componentWillMount() {
        const department = sessionStorage.getItem("department");
        this.props.loadUserRoleData(department).then(resp => {
            console.log(resp)
            let tempArr = [];
            try{
            for (let x = 0; x < resp.data.length; x++) {
                tempArr.push(resp.data[x])
            }
            if (tempArr.length > 0) {
                this.setState({ comboList: tempArr, comboValue: tempArr[0] });
                sessionStorage.setItem("role", this.state.comboValue);
            }
        }
        catch(e){
            if(e.message === "Cannot read property 'roleName' of undefined"){
                this.props.history.push("/eoffice/404")
            }
        }
        })
    }
    
    openFullScreen = () => {
        this.setState({ fullScreen: true });
        if (elem.requestFullscreen) {
            elem.requestFullscreen();
        } else if (elem.mozRequestFullScreen) {
            /* Firefox */
            elem.mozRequestFullScreen();
        } else if (elem.webkitRequestFullscreen) {
            /* Chrome, Safari & Opera */
            elem.webkitRequestFullscreen();
        } else if (elem.msRequestFullscreen) {
            /* IE/Edge */
            elem.msRequestFullscreen();
        }
    };

    closeFullScreen = () => {
        this.setState({ fullScreen: false });
        if (document.exitFullscreen) {
            document.exitFullscreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitExitFullscreen) {
            document.webkitExitFullscreen();
        } else if (document.msExitFullscreen) {
            document.msExitFullscreen();
        }
    };

    updateSidebarMode = sidebarSettings => {
        let { settings, setLayoutSettings } = this.props;
        setLayoutSettings({
            ...settings,
            layout1Settings: {
                ...settings.layout1Settings,
                leftSidebar: {
                    ...settings.layout1Settings.leftSidebar,
                    ...sidebarSettings
                }
            }
        });
    };

    handleSidebarToggle = () => {
        let { settings } = this.props;
        let { layout1Settings } = settings;

        let mode;
        if (isMdScreen()) {
            mode = layout1Settings.leftSidebar.mode === "close" ? "mobile" : "close";
        } else {
            mode = layout1Settings.leftSidebar.mode === "full" ? "close" : "full";
        }
        this.updateSidebarMode({ mode });
    };

    handleSignOut = () => {
        window.location = process.env.REACT_APP_LOGOUT_URL;
    };

    handleThemeChangeBlue = (e) => {
        let { settings } = this.props;
        let { layout1Settings } = settings;
        this.setState({lightMode:false})
        let sideBarTheme, tobBarTheme, buttonTheme;
        sideBarTheme = layout1Settings.leftSidebar.theme = 'blue';
        tobBarTheme = layout1Settings.topbar.theme = "blue";
        buttonTheme = layout1Settings.activeButton.theme = "blue";
        this.updateSidebarMode({ sideBarTheme, tobBarTheme, buttonTheme });
    };

    handleThemeChangeRed = (e) => {
        let { settings } = this.props;
        let { layout1Settings } = settings;
        this.setState({lightMode:true})
        let sideBarTheme, tobBarTheme, buttonTheme;
        sideBarTheme = layout1Settings.leftSidebar.theme = 'red';
        tobBarTheme = layout1Settings.topbar.theme = "red";
        buttonTheme = layout1Settings.activeButton.theme = 'red';
        this.updateSidebarMode({ sideBarTheme, tobBarTheme, buttonTheme });
    };

    handleChange = (event) => {
        this.setState({ comboValue: event.target.value });
        if (this.state.comboValue !== null) {
            const roleName = event.target.value;
            sessionStorage.setItem("role", roleName);
            this.refreshTables();
        }
    };

    refreshTables = () => {
        let trigger = false;
        setTimeout(() => {
            trigger = true;
            this.props.changingTableStatePA(trigger, 'CHANGE_PA_APPLICATION');
            this.props.changingTableState(trigger, 'CHANGE_PA_FILE');
            this.props.changingTableStateInbox(trigger, 'CHANGE_INBOX');
            this.props.changingTableStateOutbox(trigger, 'CHANGE_OUTBOX');
        }, 1000);
    };

    render() {
        const { fullScreen, comboList, lightMode } = this.state;
        let { theme, settings, className, style, darkState } = this.props;
        const topbarTheme = settings.themes[settings.layout1Settings.topbar.theme] || theme;
        let { layout1Settings } = settings;
        return (
            <MuiThemeProvider theme={topbarTheme}>
                <div className="topbar">
                    <div
                        className={`topbar-hold ${className}`}
                        style={Object.assign({}, { background: topbarTheme.palette.primary.main }, style)}>
                        <div className="flex flex-space-between flex-middle h-100">
                            <div className="flex">
                                <IconButton onClick={this.handleSidebarToggle} className="hide-on-lg">
                                    <Icon>menu</Icon>
                                </IconButton>
                                {
                                    layout1Settings.leftSidebar.mode === 'compact' ?
                                        (<div className="hide-on-mobile">
                                            <img src={process.env.PUBLIC_URL + '/assets/images/logo-paperless.png'} alt={"EOffice"} style={{ imageRendering: "-webkit-optimize-contrast" }} />
                                        </div>) : ''
                                }
                            </div>
                            <div className="flex flex-middle">
                                {fullScreen ? (
                                    <IconButton onClick={this.closeFullScreen}>
                                        <Tooltip title="Exit FullScreen" aria-label="Exit FullScreen"><Icon style={{ fontSize: '30px', display: 'flex' }}><img src={process.env.PUBLIC_URL + `/assets/icons/fullscreen-exit-fill.svg`} alt="Exit FullScreen" /></Icon></Tooltip>
                                    </IconButton>
                                ) : (
                                    <IconButton onClick={this.openFullScreen}>
                                        <Tooltip title="FullScreen" aria-label="FullScreen"><Icon style={{ fontSize: '30px', display: 'flex' }}><img src={process.env.PUBLIC_URL + `/assets/icons/fullscreen-fill.svg`} alt="FullScreen" /></Icon></Tooltip>
                                    </IconButton>
                                )}
                                {lightMode ? (
                                    <IconButton onClick={this.handleThemeChangeBlue}>
                                        <Tooltip title="Dark Mode" aria-label="DarkMode"><Icon style={{ fontSize: '30px', display: 'flex' }}><img src={process.env.PUBLIC_URL + `/assets/icons/sun-fill.svg`} alt="Exit FullScreen" /></Icon></Tooltip>
                                    </IconButton>
                                ) : (
                                    <IconButton onClick={this.handleThemeChangeRed}>
                                        <Tooltip title="Light Mode" aria-label="LightMode"><Icon style={{ fontSize: '30px', display: 'flex' }}><img src={process.env.PUBLIC_URL + `/assets/icons/moon-fill.svg`} alt="FullScreen" /></Icon></Tooltip>
                                    </IconButton>
                                )}
                                <FormControl className="topbarSelect" style={{ minWidth: 300, background: 'white', borderRadius: '50px', textAlignLast: 'center' }}>
                                    <Select
                                        native
                                        value={this.state.comboValue}
                                        onChange={this.handleChange}
                                        inputProps={{
                                            name: 'age',
                                            id: 'age-native-simple',
                                        }}
                                        style={{ fontSize: '16px' }}
                                    >
                                        {comboList.map(x =>
                                            <option key={x}>{x}</option>)}
                                    </Select>
                                </FormControl>
                                {/*<Switch checked={darkState} onChange={this.handleThemeChange} />*/}
                                {/* <NotificationBar /> */}
                                <Tooltip title="Logout" aria-label="Logout">
                                <IconButton onClick={this.handleSignOut}>
                                        <ExitToAppIcon color="secondary" />
                                    </IconButton>
                                    </Tooltip>
                                {/* <MatxMenu menuButton={
                                    <IconButton>
                                        <PowerSettingsNewIcon color="secondary" />
                                    </IconButton>
                                    
                                }>
                                    <MenuItem style={{ minWidth: 185 }}>
                                       <Link className="flex flex-middle" to="/">
                                           <Icon> home </Icon>
                                           <span className="pl-16"> Home </span>
                                       </Link>
                                    </MenuItem>
                                    <MenuItem style={{ minWidth: 185 }}>
                                       <Link
                                            className="flex flex-middle"
                                            to="/page-layouts/user-profile"
                                        >
                                            <Icon> person </Icon>
                                            <span className="pl-16"> Profile </span>
                                        </Link>
                                    </MenuItem>
                                    <MenuItem
                                        className="flex flex-middle"
                                        style={{ minWidth: 185 }}
                                    >
                                       <Icon> settings </Icon>
                                        <span className="pl-16"> Settings </span>
                                    </MenuItem>
                                    <MenuItem
                                        onClick={this.handleSignOut}
                                        className="flex flex-middle"
                                        style={{ minWidth: 185 }}
                                    >
                                        {/* <Icon> power_settings_new </Icon> 
                                        <span className="pl-16"> Logout </span>
                                    </MenuItem>
                                </MatxMenu> */}
                            </div>
                        </div>
                    </div>
                </div>
            </MuiThemeProvider>
        );
    }
}

Layout1Topbar.propTypes = {
    setLayoutSettings: PropTypes.func.isRequired,
    logoutUser: PropTypes.func.isRequired,
    loadUserRoleData: PropTypes.func.isRequired,
    settings: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    setLayoutSettings: PropTypes.func.isRequired,
    logoutUser: PropTypes.func.isRequired,
    loadUserRoleData: PropTypes.func.isRequired,
    settings: state.layout.settings
});

export default withStyles(styles, { withTheme: true })(
    withRouter(
        connect(mapStateToProps, { setLayoutSettings, logoutUser, loadUserRoleData, changingTableStatePA, changingTableState, changingTableStateInbox, changingTableStateOutbox })(Layout1Topbar)
    )
);
