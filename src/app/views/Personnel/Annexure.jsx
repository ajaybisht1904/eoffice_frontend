import React, { useEffect, useState } from "react";
import Paper from "@material-ui/core/Paper/Paper";
import {
    Grid as DevGrid, GroupingPanel, PagingPanel,
    Table,
    TableColumnResizing, TableEditColumn, TableEditRow,
    TableHeaderRow,
    Toolbar
} from "@devexpress/dx-react-grid-material-ui";
import {
    EditingState,
    GroupingState,
    IntegratedFiltering, IntegratedGrouping,
    IntegratedPaging, IntegratedSorting,
    PagingState, SelectionState,
    SortingState
} from "@devexpress/dx-react-grid";
import { Getter, Plugin, Template } from "@devexpress/dx-react-core";
import { Grid, Typography } from "@material-ui/core";
import { connect, useDispatch } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";
import 'react-tabs/style/react-tabs.css';
import PdfViewer from "../../pdfViewer/pdfViewer";
import DeleteIcon from '@material-ui/icons/Delete';
import AnnexureUploader from "./AnnexureUploader";
import { loadAnnexureTableData } from "../../camunda_redux/redux/action";
import { changingTableStateAnnexure } from "../../camunda_redux/redux/action/apiTriggers";
import { deleteAnnexureData } from "../../camunda_redux/redux/action";
import { setSnackbar } from "../../camunda_redux/redux/ducks/snackbar";
import { Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Button } from '@material-ui/core';
import {setPassData} from '../../camunda_redux/redux/ducks/passData';

const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            margin: theme.spacing(1),
        },
    },
    input: {
        display: 'none',
    },
    divZIndex: {
        zIndex: '1500 !important',
    },
    paperCss: {
        position: 'relative'
    },
}));

const styles = {
    customRow: {
        '&:hover': {
            backgroundColor: 'lightgray',
        }
    },
};

// functional Component begins
const Annexure = (props) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const columns = [
        { name: "pfileName", title: "File Name" },
        { name: "createdOn", title: "Date Created" }
    ];
    const [rowData, setRowData] = useState([]);
    const [selection, setSelection] = useState([1]);
    const [paID, setpaID] = useState("");
    const [fileURL, setfileURL] = useState("");
    const [open, setOpen] = useState(false);
    const [deletedRow, setDeletedRow] = useState();
    const [pdfLoads, setPdfLoads] = useState(false)

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const TableRow = ({ row, ...restProps }) => (  //custom rowComponent of DevExtreme Reactive Grid
        <Table.Row
            className={styles.customRow}
            {...restProps}
            {... { hover: true }}
            onClick={(e) => handleClick(e, row)} //event trigger on row click
            style={{
                cursor: 'pointer'
            }}
        />
    );


    const handleClick = (e, row) => { // mtd that will be performing operation on row click
        setfileURL(row.fileUrl);
        dispatch(
            setPassData(row.fileUrl)
        )
    };

    const [defaultColumnWidths] = useState([ // setting up default width of table
        { columnName: 'pfileName', width: 170 },
        { columnName: 'createdOn', width: 110 },
    ]);

    const handleChange = (event) => {
        console.log(bodyRows);
    };

    let { blnValue } = props.subscribeApi; // apitrigger that is used to update table by using redux
    useEffect(() => {
        const { fileId } = props; // Personal Inventory ID that is passed by parent component to peocess API
        setpaID(fileId);
        props.loadAnnexureTableData(fileId).then((resp) => { // API call with redux to fetch table data based on Personal Inventory ID
            let tmpArr = [];
            try {
                if (resp.data !== undefined) { // condition to check if response has data then process further
                    tmpArr = resp.data;
                    setRowData(tmpArr);
                    props.changingTableStateAnnexure(false, 'CHANGE_PA_ANNEXURE'); // redux call to change trigger to false as table got updated
                }
                else {
                    const errorMessage = resp.status + " : " + resp.error + " AT " + resp.path
                    callMessageOut(errorMessage);
                }
            }
            catch (e) {
                callMessageOut(e.message);
            }
        }).catch(error => {
            console.log(error);
        });
    }, [blnValue]);

    const callMessageOut = (message) => {
        dispatch(setSnackbar(true, "error", message));
    }
    const CustomToolbarMarkup = () => ( // table header.
        <Plugin name="customToolbarMarkup">
            <Template name="toolbarContent">
                <div style={{ marginLeft: '25%', alignSelf: 'center' }}><Typography variant='button' align='center' color='primary'>Annexure</Typography></div>
            </Template>
        </Plugin>
    );

    const CellComponent = ({ children, row, ...restProps }) => ( // Ce;; component for icons at right end of table
        <TableEditColumn.Cell row={row} {...restProps}>
            {children}
            {props.showUploader &&
            <TableEditColumn.Command
                id="delete"
                text={<DeleteIcon />}
                onExecute={() => {
                    setDeletedRow(row)
                    deleteDoc(row);
                }} // action callback
            />
            }
        </TableEditColumn.Cell>
    );

    const send = () => {
        const { sendToogle } = props;
        sendToogle(true);
    }

    // useEffect(() => {
    //     deleteDoc()
    // }, [input])

    const deleteDoc = row => { // mtd to perform delete operation 
        handleClickOpen();
    };

    const disAgreeFun = () => {
        handleClose();

    }

    const agreeFun = () => {
        handleClose();
        const row = deletedRow;
        console.log(row.id);
        const rowID = row.id;
        props.deleteAnnexureData(rowID).then((resp) => {
            dispatch( // once file has been deleted shows snackbar to notify user.
                setSnackbar(
                    true,
                    "success",
                    "Annexure has been deleted successfully !"
                )

            );
            props.changingTableStateAnnexure(true, 'CHANGE_PA_ANNEXURE');
        })
    }

    return (
        <>
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
                style={{minWidth: '300px'}}
            >
                <DialogTitle id="alert-dialog-title">{"Confirmation"}</DialogTitle>
                <DialogContent dividers>
                    <DialogContentText id="alert-dialog-description" style={{color: 'black'}}>
                        Do you want delete ANNEXURE ?
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button variant="outlined" onClick={disAgreeFun} color="primary">
                        Cencel
                    </Button>
                    <Button variant="outlined" onClick={agreeFun} color="primary" autoFocus>
                        Ok
                    </Button>
                </DialogActions>
            </Dialog>
            <div className="m-sm-20">
                <Paper elevation={3} className={classes.paperCss} >
                    <Grid container spacing={2}>
                        <Grid item xs={9}>
                            <PdfViewer personalID={""} refreshView={""} anottId={""} fileUrl={""} pdfLoads={(val) => {setPdfLoads(val)}} />
                        </Grid>
                        <Grid item xs={3}>
                            <DevGrid rows={rowData} columns={columns}>
                                <EditingState />
                                <IntegratedFiltering />
                                <PagingState
                                    defaultCurrentPage={0}
                                    pageSize={5}
                                />
                                <IntegratedPaging />
                                <SortingState />
                                <GroupingState grouping={[]} />
                                <IntegratedGrouping />
                                <IntegratedSorting />
                                <SelectionState
                                    selection={selection}
                                    onSelectionChange={handleChange}
                                />
                                <Table rowComponent={TableRow} />
                                <TableColumnResizing defaultColumnWidths={defaultColumnWidths} />
                                <TableHeaderRow showSortingControls />
                                <Toolbar />
                                <GroupingPanel />
                                <CustomToolbarMarkup />
                                <TableEditRow />
                                <TableEditColumn
                                    cellComponent={CellComponent}
                                />
                                <Getter
                                    name="tableColumns"
                                    computed={({ tableColumns }) => {
                                        const result = [
                                            ...tableColumns.filter(c => c.type !== TableEditColumn.COLUMN_TYPE),
                                            { key: 'editCommand', type: TableEditColumn.COLUMN_TYPE, width: 30 }
                                        ];
                                        return result;
                                    }
                                    }
                                />
                                <PagingPanel />
                            </DevGrid>
                            <div>
                                {props.showUploader && <AnnexureUploader personalAppID={paID} sendClick={send} />}
                                
                            </div>
                        </Grid>
                    </Grid>
                </Paper>
            </div>
        </>
    );
};

function mapStateToProps(state) {

    return {
        props: state.props,
        subscribeApi: state.subscribeApi
    };
}
export default connect(mapStateToProps, { loadAnnexureTableData, changingTableStateAnnexure, deleteAnnexureData })(Annexure);
