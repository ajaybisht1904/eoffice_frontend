import React, {useEffect, useState} from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { useFormik } from "formik";
import {connect, useDispatch} from "react-redux";
import {getPersonalInfo, updatePersonalInfo} from "../../camunda_redux/redux/action";
import * as yup from "yup";
import {setSnackbar} from "../../camunda_redux/redux/ducks/snackbar";

const useStyles1 = makeStyles((theme) => ({
    root: {
        '& > *': {
            margin: theme.spacing(1),
        },
    },
    input: {
        display: 'none',
    },
}));

function InfoForm(props) {
    const dispatch = useDispatch();
    const classes = useStyles1();
    const username = sessionStorage.getItem("username");
    const roleName = sessionStorage.getItem("role");
    const [name,setName] = useState('');
    const [email,setEmail] = useState('');
    const [address1,setAddress1] = useState('');
    const [address2,setAddress2] = useState('');
    const [address3,setAddress3] = useState('');
    const [designation,setDesignation] = useState('');

    const handleClick = (values) =>
    {
        let formData = new FormData();
        formData.append('name',values.name);
        formData.append('address1',values.address1);
        formData.append('address2',values.address2);
        formData.append('address3',values.address3);
        formData.append('email',values.email);
        formData.append('designation',values.designation);
        formData.append('username',username);
        formData.append('roleName', roleName);

        props.updatePersonalInfo(formData).then((res) => {
            try{
            if(res !== undefined){
                dispatch(setSnackbar( true, "success","Personal Information Updated Successfully!"));
                props.handleSubmit(false)
                props.disableBtn(false)
            }else{
                const errorMessage = resp.status + " : " + resp.error + " AT " + resp.path
                callMessageOut(errorMessage);
            }
        }
        catch(e){
            callMessageOut(e.message);
        }
        })
    };

    const callMessageOut = (message) => {
        dispatch(setSnackbar(true, "error",message));
    }

    useEffect(() => {
        let formData = new FormData();
        formData.append('username',username);

        props.getPersonalInfo(formData).then((res) =>
        {
            if (res.status === 'OK')
            {
                setName(res.name);
                setEmail(res.email);
                setAddress1(res.address1);
                setAddress2(res.address2);
                setAddress3(res.address3);
                setDesignation(res.designation);
            }
        })
    },[]);

    const validationSchema = yup.object({
        email: yup.string("Enter your email").email("Enter a valid email").required("Email is required"),
        name: yup.string("Enter your Name").required("Name is required"),
        address1: yup.string("Enter your Address1").required("Address1 is required"),
        address2: yup.string("Enter your Address2").required("Address2 is required"),
        address3: yup.string("Enter your Address3").required("Address3 is required"),
        designation: yup.string("Enter your Designation").required("Designation is required")
    });

    const formik = useFormik({
        initialValues: {
            name: name,
            email: email,
            address1: address1,
            address2: address2,
            address3: address3,
            designation: designation
        },
        validationSchema: validationSchema,
        enableReinitialize: true,
        onSubmit: (values) => {
            handleClick(values);
        }
    });
    return (
        <div>
            <form onSubmit={formik.handleSubmit}>
                <TextField
                    fullWidth
                    id="name"
                    name="name"
                    label="Name"
                    value={formik.values.name}
                    onChange={formik.handleChange}
                    error={formik.touched.name && Boolean(formik.errors.name)}
                    helperText={formik.touched.name && formik.errors.name}
                />
                <TextField
                    fullWidth
                    id="email"
                    name="email"
                    label="Email"
                    value={formik.values.email}
                    onChange={formik.handleChange}
                    error={formik.touched.email && Boolean(formik.errors.email)}
                    helperText={formik.touched.email && formik.errors.email}
                />
                <TextField
                    fullWidth
                    id="designation"
                    name="designation"
                    label="Designation"
                    value={formik.values.designation}
                    onChange={formik.handleChange}
                    error={
                        formik.touched.designation && Boolean(formik.errors.designation)
                    }
                    helperText={formik.touched.designation && formik.errors.designation}
                />
                <TextField
                    fullWidth
                    id="address1"
                    name="address1"
                    label="Address Line 1"
                    multiline
                    value={formik.values.address1}
                    onChange={formik.handleChange}
                    error={formik.touched.address1 && Boolean(formik.errors.address1)}
                    helperText={formik.touched.address1 && formik.errors.address1}
                />
                <TextField
                    fullWidth
                    id="address2"
                    name="address2"
                    label="Address Line 2"
                    multiline
                    value={formik.values.address2}
                    onChange={formik.handleChange}
                    error={formik.touched.address2 && Boolean(formik.errors.address2)}
                    helperText={formik.touched.address2 && formik.errors.address2}
                />
                <TextField
                    fullWidth
                    id="address3"
                    name="address3"
                    label="Address Line 3"
                    multiline
                    value={formik.values.address3}
                    onChange={formik.handleChange}
                    error={formik.touched.address3 && Boolean(formik.errors.address3)}
                    helperText={formik.touched.address3 && formik.errors.address3}
                />
                <Grid item style={{textAlignLast: 'end', marginTop: '10px'}}>
                <Button color="primary" variant="outlined" type="submit">
                    Update
                </Button>
                </Grid>
            </form>
        </div>
    );
}
function mapStateToProps(state) {

    return { props: state.props };
}
export default connect(mapStateToProps,{getPersonalInfo,updatePersonalInfo})(InfoForm);
