import React, {Component} from "react";
import {Button, ButtonGroup, Dialog, DialogContent, DialogTitle, Grid, IconButton, Typography} from "@material-ui/core";
import {withStyles} from "@material-ui/styles";
import PersonalAppTable from "./PersonalAppTable";
import PersonalFileTable from "./PersonalFileTable";
import DraftPaFileTable from "./DraftPaTable";
import {Breadcrumb} from "../../../matx";
import StartProcessPage from "../initiate/shared/startProcess/StartProcessPage";
import CloseIcon from '@material-ui/icons/Close';
import InfoForm from "./InfoForm";
import {Loading} from "./therme-source/material-ui/loading"
import {getPersonalInfo} from '../../camunda_redux/redux/action'
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { PropTypes } from "prop-types";


const styles = theme => ({})

class Personnel extends Component {
    state = { // initializing state of class component
        open: false,
        openPA: false,
        openInfo:false,
        loading: false,
        blnDisableButtoms: false
    };

    componentDidMount(){
        const username = sessionStorage.getItem("username");
        let formData = new FormData();
        formData.append('username',username);

        this.props.getPersonalInfo(formData).then((res) =>
        {
            if (res.status === 'OK')
            {
               this.setState({blnDisableButtoms:false})
            }else{
                this.setState({blnDisableButtoms:true})
            }
        })
    }

    handleCloseEvent = (e) => {  // callback function that fires when record of Personal File has been saved
        this.setState({open : e});
    };

    handleCloseEventPA = (e) => { // callback function that fires when record of Personal File has been saved
        this.setState({openPA : e});
    };
    
    render(){
        const {loading, blnDisableButtoms} = this.state
        return(
            <div className="m-sm-30">
                <Grid container justifyContent="center" spacing={2}>
                    <Grid item xs={12} sm={4}>
                        <Breadcrumb
                            routeSegments={[
                                { name: "Personnel", path: "/personnel/file" }
                            ]}
                        />
                    </Grid>
                    <Grid item xs={12} sm={8} style={{display: 'flex', columnGap: '15px', marginBottom: '10px'}} className="personnel-btn-groups">
                                                   
                            <Button variant="outlined" color="primary" disabled={blnDisableButtoms} onClick={()=> this.setState({openPA : true})} style={{background: '#FFAF38', letterSpacing: '2px', fontWeight: 'bold', color: 'white', boxShadow: 'rgba(0, 0, 0, 0.35) 0px 5px 15px', marginTop: '10px'}}>Personal Application</Button>
                            <Button variant="outlined" color="primary" disabled={blnDisableButtoms} onClick={() => this.setState({open : true})} style={{background: '#FFAF38', letterSpacing: '2px', fontWeight: 'bold', color: 'white', boxShadow: 'rgba(0, 0, 0, 0.35) 0px 5px 15px', marginTop: '10px'}}>Personal File</Button>
                            <Button variant="outlined" color="primary" onClick={() => this.setState({openInfo : true})} style={{background: '#FFAF38', letterSpacing: '2px', fontWeight: 'bold', color: 'white', boxShadow: 'rgba(0, 0, 0, 0.35) 0px 5px 15px', marginTop: '10px'}}>My Info</Button>
                    </Grid>
                    {blnDisableButtoms && <Grid item xs={12} style={{textAlign: 'center'}}>
                        <Typography component="h4"><span style={{color: 'red'}}>***Please update <b>My Info</b> before further processing.***</span></Typography>
                    </Grid>}
                </Grid>
                <Grid container justifyContent="center" spacing={2}>
                    <Grid item md={3} sm={6} xs={12} className="personal-file">
                        <PersonalFileTable />
                    </Grid>
                    <Grid item md={4} sm={6} xs={12} className="personal-file">
                        <DraftPaFileTable blnEnableLoader={(val) => this.setState({loading : val})} />
                    </Grid>
                    <Grid item md={5} xs={12} className="personal-file">
                        <PersonalAppTable blnEnableLoader={(val) => this.setState({loading : val})} />
                    </Grid>
                    {loading && <Loading />}
                </Grid>
                <div>
                    <Dialog
                        open={this.state.open}
                        aria-labelledby="draggable-dialog-title"
                        maxWidth='sm'
                    >
                        <DialogTitle id="draggable-dialog-title" style={{padding: "0px 24px !important"}}>
                            Create A Personal File
                            <IconButton aria-label="close" onClick={() =>  this.setState({open : false})} color="primary" style={{float: 'right'}}>
                                <CloseIcon />
                            </IconButton>
                        </DialogTitle>
                        <DialogContent dividers  pt={0}>
                            <StartProcessPage process={'personalFile'} handleCloseEvent={this.handleCloseEvent} didMounting={this.mountData}/>
                        </DialogContent>
                    </Dialog>
                    <Dialog
                        open={this.state.openPA}
                        aria-labelledby="draggable-dialog"
                        maxWidth='sm'
                    >
                        <DialogTitle id="draggable-dialog" style={{padding: "0px 24px !important"}}>
                            Create A Personal Application
                            <IconButton aria-label="close" onClick={() => this.setState({openPA : false})} color="primary" style={{float: 'right'}}>
                                <CloseIcon />
                            </IconButton>
                        </DialogTitle>
                        <DialogContent dividers  pt={0}>
                            <StartProcessPage process={'personalApplication'} handleCloseEvent={this.handleCloseEventPA} didMounting={this.mountData}/>
                        </DialogContent>
                    </Dialog>
                    <Dialog
                        open={this.state.openInfo}
                        aria-labelledby="draggable-dialog"
                        maxWidth='sm'
                    >
                        <DialogTitle id="draggable-dialog" style={{padding: "0px 24px !important"}}>
                            Personal Information
                            <IconButton aria-label="close" onClick={() => this.setState({openInfo : false})} color="primary" style={{float: 'right'}}>
                                <CloseIcon />
                            </IconButton>
                        </DialogTitle>
                        <DialogContent dividers  pt={0}>
                            <InfoForm handleSubmit={(val) => this.setState({openInfo: val})} disableBtn={(val) => this.setState({blnDisableButtoms: val})}/>
                        </DialogContent>
                    </Dialog>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    getPersonalInfo: PropTypes.func.isRequired,
})
export default(
    withRouter(
        connect(mapStateToProps,{getPersonalInfo})(Personnel)
    )
);
