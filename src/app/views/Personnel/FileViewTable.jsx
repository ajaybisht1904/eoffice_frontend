import React, {useEffect, useState} from "react";
import Paper from "@material-ui/core/Paper/Paper";
import {
    Grid as DevGrid, GroupingPanel, PagingPanel,
    Table,
    TableColumnResizing,
    TableHeaderRow,
    Toolbar
} from "@devexpress/dx-react-grid-material-ui";
import {
    GroupingState,
    IntegratedFiltering, IntegratedGrouping,
    IntegratedPaging, IntegratedSorting,
    PagingState, SelectionState,
    SortingState
} from "@devexpress/dx-react-grid";
import {Plugin, Template} from "@devexpress/dx-react-core";
import {Grid, Typography} from "@material-ui/core";
import {loadPFData} from '../../camunda_redux/redux/action';
import {connect} from "react-redux";
import PdfViewer from "../../pdfViewer/pdfViewer";
import {useDispatch} from "react-redux";
import { setSnackbar } from "app/camunda_redux/redux/ducks/snackbar";

const styles = {
    customRow: {
        '&:hover': {
            backgroundColor: 'lightgray',
        }
    },
};

const FileViewTable = (props) => {
    const dispatch = useDispatch();
    const columns = [
        { name: "fileName", title: "#" },
        { name: "subject", title: "Subject" }
    ];

    const [rowData,setRowData] = useState([]);
    const [selection, setSelection] = useState([1]);
    const [pdfLoads, setPdfLoads] = useState(false)
    const TableRow = ({ row, ...restProps }) => (
        <Table.Row
            className={styles.customRow}
            {...restProps}
            {... {hover: true}}
            // eslint-disable-next-line no-alert
            onClick={(e) => handleClick(e, row)}
            style={{
                cursor: 'pointer'
            }}
        />
    );

    const handleClick = (e, row) => {
        console.log(row);
    };
    const username = sessionStorage.getItem("username");
    const [defaultColumnWidths] = useState([
        { columnName: 'fileName', width: 160 },
        { columnName: 'subject', width: 180 },
    ]);

    const handleChange = (event) => {
        console.log(bodyRows);

    };

    useEffect(async() => {
        props.loadPFData(username).then((resp) => {
            let tmpArr =[];
            try
            {
            if (resp.data !== undefined) {
                tmpArr = resp.data;
                setRowData(tmpArr);
            } else{
                const errorMessage = resp.status + " : " + resp.error + " AT " + resp.path
                callMessageOut(errorMessage);
            }
        }
        catch(e){
            callMessageOut(e.message)
        }
        }).catch(error => {
            console.log(error);
        });
    },[]);

    const callMessageOut = (message) => {
        dispatch(setSnackbar(true, "error",message));
    }

    const CustomToolbarMarkup = () => (
        <Plugin name="customToolbarMarkup">
            <Template name="toolbarContent">
                <div style={{marginLeft: '40%', alignSelf: 'center'}}><Typography variant='button' align='center' color='primary'>Personal File</Typography></div>
            </Template>
        </Plugin>
    );

    return (
        <div className="m-sm-30">
            <Grid container spacing={2}>
            <Grid item xs={8}>
                        <PdfViewer fileUrl={""} pdfLoads={(val) => {setPdfLoads(val)}} />
                    </Grid>
                    <Grid item xs={4}>
                    <Paper elevation={3} style={{ position: 'relative' }}>
                        <DevGrid rows={rowData} columns={columns}>
                            <IntegratedFiltering />
                            <PagingState
                                defaultCurrentPage={0}
                                pageSize={5}
                            />
                            <IntegratedPaging />
                            <SortingState />
                            <GroupingState grouping={[]} />
                            <IntegratedGrouping />
                            <IntegratedSorting/>
                            <SelectionState
                                selection={selection}
                                onSelectionChange={handleChange}
                            />
                            <Table rowComponent={TableRow} />
                            <TableColumnResizing defaultColumnWidths={defaultColumnWidths}/>
                            <TableHeaderRow showSortingControls/>
                            <Toolbar />
                            <GroupingPanel />
                            <CustomToolbarMarkup />
                            <PagingPanel />
                        </DevGrid>
                        </Paper>
                    </Grid>
                    
            </Grid>
        </div>
    );
};

function mapStateToProps(state) {

    return { props: state.props };
}

export default connect(mapStateToProps,{loadPFData})(FileViewTable);