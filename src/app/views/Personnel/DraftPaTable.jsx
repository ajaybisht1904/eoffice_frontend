import React, {useState, useEffect} from "react";
import {Card, Dialog, DialogContent, DialogTitle, Grid, IconButton, Paper, Slide} from "@material-ui/core";
import {
    Grid as DevGrid, GroupingPanel, PagingPanel,
    Table,
    TableColumnResizing,
    TableHeaderRow,
    Toolbar,
    TableFilterRow
} from "@devexpress/dx-react-grid-material-ui";
import {
    GroupingState,
    IntegratedFiltering, IntegratedGrouping,
    CustomPaging, IntegratedSorting,
    PagingState, SelectionState,
    SortingState,FilteringState
} from "@devexpress/dx-react-grid";
import {Plugin, Template} from "@devexpress/dx-react-core";
import {Typography} from "@material-ui/core";
import {setSnackbar} from '../../camunda_redux/redux/ducks/snackbar';
import { loadPADraftTableData, loadSfdt } from '../../camunda_redux/redux/action';
import { connect as reduxConnect, useDispatch } from "react-redux";
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
import Annexure from "./Annexure";
import InputForm from "./quickSignFrom";
import { makeStyles } from "@material-ui/core/styles";
import CloseIcon from '@material-ui/icons/Close';
import HeadersAndFootersView from "../FileApproval/documentEditor/editor";
import StartProcessPage from "../initiate/shared/startProcess/StartProcessPage";
import {changeTableStateDraft} from '../../camunda_redux/redux/action/apiTriggers';

const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            margin: theme.spacing(1),
        },
    },
    input: {
        display: 'none',
    },
    divZIndex: {
        zIndex: '0',

    },
    gridHeader: {
        fontSize: '22px',
        textAlign: 'center',
        margin: '3% 0',
    },
}));


const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

const DraftPaFileTable = (props) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const columns = [
        { name: "createdOn", title: "Date Created" },
        { name: "pfileName", title: "PA Number" },
        { name: "subject", title: "PA Subject" },
    ];

    const [rowData,setRowData] = useState([]);
    const [selection, setSelection] = useState([1]);
    const [openQuickSign, setOpenQuickSign] = useState(false);
    const [open, setOpen] = useState(false);
    const [blnOpenQuickSign, setblnOpenQuickSign] = useState(false);
    const [blnOpenEditor, setblnOpenEditor] = useState(true);
    const [tabIndex, setTabIndex] = useState(0);
    const [rowID, setRowID] = useState("");
    const [fileURL, setFileURL] = useState("");
    let username = sessionStorage.getItem("username");
    let role = sessionStorage.getItem("role");
    let dept = sessionStorage.getItem("department");
    const [pageSize] = useState(10);
    const [currentPage, setCurrentPage] = useState(0);
    const [totalCount, setTotalCount] = useState(0);

    const TableRow = ({ row, ...restProps }) => (
        <Table.Row
            {...restProps}
            {... {hover: true}}
            onClick={(e) => handleClick(e, row)}
            style={{
                cursor: 'pointer',
                '&:hover': {
                    backgroundColor: 'lightgray',
                }
            }}
        />
    );
    const { blnValueDraft } = props.subscribeApi;
    useEffect(() => pADraftTableData(),[blnValueDraft,currentPage]);

    const pADraftTableData = () => {
        setRowData([]);
            props.loadPADraftTableData(username, role,pageSize, currentPage).then((resp) => {
                let tmpArr = [];
                try{
                if (resp) { // condition to check if response then perform further 
                    setTotalCount(resp.length);
                    console.log(resp)
                    if (resp.data !== undefined) {
                        tmpArr = resp.data;
                        setRowData(tmpArr);
                    }
                    else{
                        const errorMessage = resp.status + " : " + resp.error + " AT " + resp.path
                        callMessageOut(errorMessage);
                    }
                   
                   props.changeTableStateDraft(false, 'CHANGE_PA_DRAFT');
                }
            }
            catch(e){
                callMessageOut(e.message)
            }
            }).catch(error => {
                console.log(error);
            });
    }

    const callMessageOut = (message) => {
        dispatch(setSnackbar(true, "error", message));
    }

    const loadSFDT = (url, id) => {
        props.blnEnableLoader(true);
        props.loadSfdt(url, username, id, role, dept) // API call to load sfdt which will be visible on sincfusion
            .then(response => {
                try {
                    const URL = response.url;
                    setRowID(id);
                    setFileURL(URL);
                    if (URL) {
                        props.blnEnableLoader(false);
                        setblnOpenQuickSign(false)
                        setblnOpenEditor(true)
                        setTabIndex(0)
                        setOpenQuickSign(true);
                    }
                }
                catch (e) {
                    callMessageOut(e.message);
                }
            })

    }

    const [defaultColumnWidths] = useState([
        { columnName: 'createdOn', width: 140 },
        { columnName: 'pfileName', width: 120 },
        { columnName: 'subject', width: 180 },
    ]);

    const handleChange = (event) => {};

    const handleClick = (e, row) => { // Mtd to perform operation while row clicks
        const url = row.fileURL;
        sessionStorage.setItem("FileURL", url);
        loadSFDT(url, row.id);

    };

    const handleClickQuickSignClose = () => { // Mtd that triggers while clicking on close icon of quick sign dialog
        setOpenQuickSign(false);
        setblnOpenEditor(true);
        setblnOpenQuickSign(false);
    };

    const CustomToolbarMarkup = () => (
        <Plugin name="customToolbarMarkup">
            <Template name="toolbarContent">
                <div style={{marginLeft: '15%', alignSelf: 'center'}}><Typography variant='button' align='center' color='primary'>Draft Personal Application</Typography></div>
            </Template>
        </Plugin>
    );

    const [tableColumnExtensions] = useState([
        { columnName: 'subject',  wordWrapEnabled: true },
    ])
    return (
        <div>
            <Paper elevation={3} style={{ position: 'relative' }}>
                <DevGrid rows={rowData} columns={columns}>
                    <FilteringState />
                    <IntegratedFiltering />
                    <PagingState
                        currentPage={currentPage}
                        onCurrentPageChange={setCurrentPage}
                        pageSize={pageSize}
                    />
                    <CustomPaging
                        totalCount={totalCount}
                    />
                    <SortingState />
                    <GroupingState grouping={[]} />
                    <IntegratedGrouping />
                    <IntegratedSorting/>
                    <SelectionState
                        selection={selection}
                        onSelectionChange={handleChange}
                    />
                    <Table rowComponent={TableRow} columnExtensions={tableColumnExtensions} />
                    <TableColumnResizing defaultColumnWidths={defaultColumnWidths}/>
                    <TableHeaderRow showSortingControls/>
                    <TableFilterRow />
                    <Toolbar />
                    <GroupingPanel />
                    <CustomToolbarMarkup />
                    <PagingPanel />
                </DevGrid>
            </Paper>
            <Dialog
                open={openQuickSign}
                onClose={handleClickQuickSignClose}
                fullScreen
                aria-labelledby="quickSignDialog"
                TransitionComponent={Transition}
                className={classes.divZIndex}
            >
                <DialogContent dividers>
                    <Tabs forceRenderTabPanel selectedIndex={tabIndex} onSelect={index => setTabIndex(index)}>
                        <TabList>
                            <Tab style={{ borderRadius: '5px 5px 0 0'}}>PERSONAL APPLICATION</Tab>
                            <Tab style={{borderRadius: '5px 5px 0 0'}} >ANNEXURE</Tab>
                            <Tab style={{borderRadius: '5px 5px 0 0'}} >QUICK SIGN</Tab>
                            <Tab style={{ borderRadius: '5px 5px 0 0' }}>SEND</Tab>
                            <IconButton aria-label="close" onClick={handleClickQuickSignClose} style={{ borderRadius: '10px', float: 'right', height: '35px', width: '35px', color: 'blue', borderColor: 'blue', borderWidth: '1px', borderStyle: 'revert' }}>
                                <CloseIcon />
                            </IconButton>
                        </TabList>

                        <TabPanel>
                            <div hidden={!blnOpenQuickSign}>
                                <InputForm sendToogle={e => {setTabIndex(3)}} fileId={rowID} returnToEditor={e => {setblnOpenQuickSign(false); setblnOpenEditor(true) }} />
                            </div>
                            <div hidden={!blnOpenEditor}>
                                <HeadersAndFootersView sendToogle={e => {setTabIndex(3)}} fileId={rowID} blnIsPartCase={false} fileUrl1={fileURL} blnOpenQuickSign={e => {setblnOpenQuickSign(true); setblnOpenEditor(false);}} blnShowQuickSign={true} />
                            </div>

                        </TabPanel>
                        <TabPanel>
                            <Annexure fileId={rowID} sendToogle={e => {setTabIndex(3)}} showUploader={true} />
                        </TabPanel>
                        <TabPanel>
                        <InputForm sendToogle={e => {setTabIndex(3)}} fileId={rowID} returnToEditor={e => {setblnOpenQuickSign(false); setblnOpenEditor(true) }} />
                        </TabPanel>
                        <TabPanel>
                            <Grid container justifyContent='center'>
                                <Grid item xs={12}>
                                    <Typography color='primary' className={classes.gridHeader}>Forward File for Review Approval</Typography>
                                </Grid>
                            </Grid>
                            <Grid container>
                                <Grid item xs={3}></Grid>
                                <Grid item xs={6}>
                                    <Card style={{padding: '25px'}} elevation={5}>
                                        <StartProcessPage process={'sendFile'} fileId={rowID} handleCloseEvent={e => {setOpen(false); setOpenQuickSign(false)}} />
                                    </Card>
                                </Grid>
                                <Grid item xs={3}></Grid>
                            </Grid>
                        </TabPanel>
                    </Tabs>
                </DialogContent>
            </Dialog>
        </div>
    );
}

function mapStateToProps(state) {

    return {
        props: state.props,
        subscribeApi: state.subscribeApi,
    };
}

export default reduxConnect(mapStateToProps, {loadPADraftTableData, loadSfdt, changeTableStateDraft})(DraftPaFileTable);
