import React, { useEffect, useState } from "react";
import Paper from "@material-ui/core/Paper/Paper";
import {
    Grid as DevGrid, GroupingPanel, PagingPanel,
    Table,
    TableColumnResizing,
    TableHeaderRow,
    Toolbar,
    TableFilterRow
} from "@devexpress/dx-react-grid-material-ui";
import {
    GroupingState,
    IntegratedFiltering, IntegratedGrouping,
    CustomPaging, IntegratedSorting,
    PagingState, SelectionState,
    SortingState, FilteringState
} from "@devexpress/dx-react-grid";
import { Plugin, Template } from "@devexpress/dx-react-core";
import { Dialog, DialogContent, DialogTitle, Icon, IconButton, Tooltip, Typography } from "@material-ui/core";
import { loadPATableData, loadSfdt } from '../../camunda_redux/redux/action';
import { connect, useDispatch } from "react-redux";
import HeadersAndFootersView from "../FileApproval/documentEditor/editor";
import Slide from "@material-ui/core/Slide/Slide";
import { makeStyles } from "@material-ui/core/styles";
import CloseIcon from '@material-ui/icons/Close';
import StartProcessPage from "../initiate/shared/startProcess/StartProcessPage";
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
import Annexure from "./Annexure";
import InputForm from "./quickSignFrom";
import { changingTableStatePA, changeTableStateDraft } from "../../camunda_redux/redux/action/apiTriggers";
import { setRefresh } from "../../redux/actions/RefreshActions";
import { setSnackbar } from "app/camunda_redux/redux/ducks/snackbar";
import './therme-source/material-ui/loading.css';
import { Timeline, TimelineConnector, TimelineContent, TimelineDot, TimelineItem, TimelineOppositeContent, TimelineSeparator } from "@material-ui/lab";
import PlayCircleOutlineIcon from '@material-ui/icons/PlayCircleOutline';
import LaptopMacIcon from '@material-ui/icons/LaptopMac';
import ForwardIcon from '@material-ui/icons/Forward';
import RepeatIcon from '@material-ui/icons/Repeat';

const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            margin: theme.spacing(1),
        },
    },
    input: {
        display: 'none',
    },
    divZIndex: {
        zIndex: '0',

    },
    gridHeader: {
        fontSize: '22px',
        textAlign: 'center',
        margin: '3% 0',
    },
    paper: {
        padding: '6px 16px',
    },
    secondaryTail: {
        backgroundColor: theme.palette.secondary.main,
    },
}));

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

const PersonalAppTable = (props) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    // All state variables initialization
    const columns = [
        { name: "subject", title: "Subject" },
        { name: "createdOn", title: "Date Applied" },
        { name: "status", title: "Status" },
        { name: "pfileName", title: "Application #" },
    ];
    const [tabIndex, setTabIndex] = useState(0);
    const [rowData, setRowData] = useState([]);
    const [selection, setSelection] = useState([1]);
    const [openQuickSign, setOpenQuickSign] = useState(false);
    const [open, setOpen] = useState(false);
    const [rowID, setRowID] = useState("");
    const [fileURL, setFileURL] = useState("");
    const [blnOpenQuickSign, setblnOpenQuickSign] = useState(false);
    const [blnOpenEditor, setblnOpenEditor] = useState(true);
    const [blnOpenHistory, setblnOpenHistory] = useState(false);
    let username = sessionStorage.getItem("username");
    let role = sessionStorage.getItem("role");
    let dept = sessionStorage.getItem("department");
    const [pageSize] = useState(10);
    const [currentPage, setCurrentPage] = useState(0);
    const [totalCount, setTotalCount] = useState(0);


    const TableRow = ({ row, ...restProps }) => ( // Custom table row template
        <Table.Row
            {...restProps}
            {... { hover: true }}
            // eslint-disable-next-line no-alert
            onClick={(e) => handleClick(e, row)}
            style={{
                cursor: 'pointer',
                '&:hover': {
                    backgroundColor: 'lightgray',
                }
            }}
        />
    );

    const sampleData = [
        {
            "id": 1,
            "typo": '02-09-2021 9:30 am',
            "icon": PlayCircleOutlineIcon,
            "title": 'File Generated',
            "description": 'Forwarded to HRC',
            "background": 'yellowgreen'
        },
        {
            "id": 2,
            "typo": '03-09-2021 10:00 am',
            "icon": LaptopMacIcon,
            "title": 'File Reviewed',
            "description": 'Made Part Case',
            "color": 'primary',
            "background": "orange"
        },
        {
            "id": 3,
            "typo": '04-09-2021 10:00 pm',
            "icon": ForwardIcon,
            "title": 'File Forwarded',
            "description": 'Forwarded for further approval',
            "color": "primary",
            "variant": "outlined",
            "background": "turquoise"
        },
        {
            "id": 4,
            "typo": '',
            "icon": RepeatIcon,
            "title": 'On Hold',
            "description": 'For clarification',
            "color": "secondary",
            "background": "salmon"
        },
    ]

    const handleClick = (e, row) => { // Mtd to perform operation while row clicks
        const url = row.fileURL;
        sessionStorage.setItem("FileURL", url);
        loadSFDT(url, row.id);

    };

    const loadSFDT = (url, id) => {
        props.blnEnableLoader(true);
        props.loadSfdt(url, username, id, role, dept) // API call to load sfdt which will be visible on sincfusion
            .then(response => {
                try {
                    const URL = response.url;
                    setRowID(id);
                    setFileURL(URL);
                    if (URL) {
                        props.blnEnableLoader(false);
                        setblnOpenQuickSign(false)
                        setblnOpenEditor(true)
                        setTabIndex(0)
                        setOpenQuickSign(true);
                    }
                }
                catch (e) {
                    callMessageOut(e.message);
                }
            })

    }

    const [defaultColumnWidths, setDefaultColumnWidths] = useState([ // Default column width of table
        { columnName: 'subject', width: 230 },
        { columnName: 'createdOn', width: 120 },
        { columnName: 'pfileName', width: 130 },
        { columnName: 'status', width: 100 },
    ]);
    const handleChange = (event) => {
        console.log(bodyRows);
    };

    const { blnValuePA } = props.subscribeApi;
    useEffect(() => loadPersonalApplicationTable(), [blnValuePA, currentPage]);

    const loadPersonalApplicationTable = () => {
        props.blnEnableLoader(true);
        setRowData([]);
        props.loadPATableData(username, role,pageSize, currentPage).then((resp) => {
            try {
                let tmpArr = [];
                if (resp) {
                    setTotalCount(resp.length);
                    if (resp.data !== undefined) {
                        tmpArr = resp.data;
                        setRowData(tmpArr);
                    }
                    else {
                        const errorMessage = resp.status + " : " + resp.error + " AT " + resp.path
                        callMessageOut(errorMessage);
                    }
                    props.changingTableStatePA(false, 'CHANGE_PA_APPLICATION');
                    props.changeTableStateDraft(false, 'CHANGE_PA_DRAFT');
                }
                props.blnEnableLoader(false);
            }
            catch (e) {
                callMessageOut(e.message)
            }
        }).catch(error => {
            props.blnEnableLoader(false);
            console.log(error);
        });
    }

    const callMessageOut = (message) => {
        dispatch(setSnackbar(true, "error", message));
    }

    const handleClickQuickSignClose = () => { // Mtd that triggers while clicking on close icon of quick sign dialog
        setOpenQuickSign(false);
        setblnOpenEditor(true);
        setblnOpenQuickSign(false);
    };

    const handleOnClickOpenHistory = (e, row) => {
        e.stopPropagation();
        setblnOpenHistory(true)
        console.log(row)
    }

    const CustomToolbarMarkup = () => ( // Custom table header of table 
        <Plugin name="customToolbarMarkup">
            <Template name="toolbarContent">
                <div style={{ marginLeft: '25%', alignSelf: 'center' }}><Typography variant='button' align='center' color='primary'>My Personal Applications</Typography></div>
            </Template>
        </Plugin>
    );

    const [tableColumnExtensions] = useState([
        { columnName: 'subject', align: 'left', width: '40%', wordWrapEnabled: true },
    ])

    const Cell = ({ row, column, ...props }) => {
        if (column.name === "pfileName")
            return (
                <Table.Cell {...props} style={{ paddingLeft: '0px' }} >
                    <span style={{ marginRight: '8px' }}>{row.pfileName}</span>
                    <IconButton aria-label="userHistory" size="small" onClick={(e) => { handleOnClickOpenHistory(e, row) }} >
                        <Tooltip title="Show User History"
                            aria-label="Show User History">
                            <Icon style={{ height: 'auto', fontSize: '1.40rem', marginTop: '-5px' }}><img src={process.env.PUBLIC_URL + `/assets/icons/chat-history-line.svg`}
                                alt="Show User History" /></Icon></Tooltip>
                    </IconButton>

                </Table.Cell>
            );
        return <Table.Cell {...props} />;
    };


    return (
        <div>
            <Paper elevation={3} style={{ position: 'relative' }}>
                <DevGrid rows={rowData} columns={columns}>
                    <FilteringState />
                    <IntegratedFiltering />
                    <PagingState
                        currentPage={currentPage}
                        onCurrentPageChange={setCurrentPage}
                        pageSize={pageSize}
                    />
                    <CustomPaging
                        totalCount={totalCount}
                    />
                    <SortingState />
                    <GroupingState grouping={[]} />
                    <IntegratedGrouping />
                    <IntegratedSorting />
                    <SelectionState
                        selection={selection}
                        onSelectionChange={handleChange}
                    />
                    <Table rowComponent={TableRow} cellComponent={Cell} columnExtensions={tableColumnExtensions} />
                    <TableColumnResizing columnWidths={defaultColumnWidths} onColumnWidthsChange={setDefaultColumnWidths} resizingMode="nextColumn" />
                    <TableHeaderRow showSortingControls />
                    <TableFilterRow />
                    <Toolbar />
                    <GroupingPanel />
                    <CustomToolbarMarkup />
                    <PagingPanel />
                </DevGrid>

            </Paper>
            <Dialog
                open={blnOpenHistory}
                onClose={(e) => setblnOpenHistory(false)}
                aria-labelledby="alert-user-history"
                aria-describedby="alert-dialog-description"
                style={{ minWidth: '300px' }}
            >
                <DialogTitle id="alert-user-history">User History</DialogTitle>
                <DialogContent dividers style={{ maxHeight: '600px' }}>
                    <Timeline align="alternate">
                        {sampleData.map(item =>
                            <TimelineItem key={item.id}>
                                <TimelineOppositeContent>
                                    <Typography variant="body2" color="textSecondary">
                                        {item.typo}
                                    </Typography>
                                </TimelineOppositeContent>
                                <TimelineSeparator>
                                    <TimelineDot color={item.color} variant={item.variant}>
                                        <item.icon />
                                    </TimelineDot>
                                    <TimelineConnector />
                                </TimelineSeparator>
                                <TimelineContent>
                                    <Paper elevation={3} className={classes.paper} style={{ backgroundColor: item.background }}>
                                        <Typography variant="h6" component="h1">
                                            {item.title}
                                        </Typography>
                                        <Typography>{item.description}</Typography>
                                    </Paper>
                                </TimelineContent>
                            </TimelineItem>
                        )}

                    </Timeline>
                </DialogContent>
            </Dialog>
            <Dialog
                open={openQuickSign}
                onClose={handleClickQuickSignClose}
                fullScreen
                aria-labelledby="quickSignDialog"
                TransitionComponent={Transition}
                className={classes.divZIndex}
            >
                <DialogContent dividers>
                    <Tabs forceRenderTabPanel selectedIndex={tabIndex} onSelect={index => setTabIndex(index)}>
                        <TabList>
                            <Tab style={{ borderRadius: '5px 5px 0 0' }}>PERSONAL APPLICATION</Tab>
                            <IconButton aria-label="close" onClick={handleClickQuickSignClose} style={{ borderRadius: '10px', float: 'right', height: '35px', width: '35px', color: 'blue', borderColor: 'blue', borderWidth: '1px', borderStyle: 'revert' }}>
                                <CloseIcon />
                            </IconButton>
                        </TabList>

                        <TabPanel>
                            <div hidden={!blnOpenEditor}>

                                <Annexure fileId={rowID} showUploader={false} />
                            </div>

                        </TabPanel>

                    </Tabs>
                </DialogContent>
            </Dialog>
        </div>
    );
};

function mapStateToProps(state) {

    return {
        props: state.props,
        subscribeApi: state.subscribeApi,
        refreshings: state.refreshings
    };
}
export default connect(mapStateToProps, { setRefresh, loadPATableData, loadSfdt, changingTableStatePA, changeTableStateDraft })(PersonalAppTable);
