import React, { useEffect, useState } from "react";
import Paper from "@material-ui/core/Paper/Paper";
import {
    Grid as DevGrid, GroupingPanel, PagingPanel,
    Table,
    TableColumnResizing,
    TableHeaderRow,
    Toolbar,
    TableFilterRow
} from "@devexpress/dx-react-grid-material-ui";
import {
    FilteringState,
    GroupingState,
    IntegratedFiltering, IntegratedGrouping,
    IntegratedSorting,
    PagingState, SelectionState,
    SortingState, CustomPaging
} from "@devexpress/dx-react-grid";
import { Plugin, Template } from "@devexpress/dx-react-core";
import { Typography } from "@material-ui/core";
import { loadPFData } from '../../camunda_redux/redux/action';
import { connect, useDispatch } from "react-redux";
import history from '../../../history'
import { changingTableState } from "../../camunda_redux/redux/action/apiTriggers";
import { setRefresh1 } from "../../redux/actions/Refresh1Actions";
import { setSnackbar } from "app/camunda_redux/redux/ducks/snackbar";

const PersonalFileTable = (props) => {
    //Initialization of state variables
    const dispatch = useDispatch();
    const columns = [
        { name: "fileName", title: "#" },
        { name: "subject", title: "Subject" },

    ];
    const [rowData, setRowData] = useState([]);
    const [selection, setSelection] = useState([1]);
    const username = sessionStorage.getItem("username");
    const role = sessionStorage.getItem("role");
    const [pageSize] = useState(10);
    const [currentPage, setCurrentPage] = useState(0);
    const [totalCount, setTotalCount] = useState(0);


    const TableRow = ({ row, ...restProps }) => ( // custom table row to perform row click operation
        <Table.Row
            // className={styles.customRow}
            {...restProps}
            {... { hover: true }}
            // eslint-disable-next-line no-alert
            onClick={(e) => handleClick(e, row)}
            style={{
                cursor: 'pointer',
                '&:hover': {
                    backgroundColor: 'lightgray',
                }
            }}
        />
    );

    const handleClick = (e, row) => { // mtd that has been triggered while row clicks
        if (row !== undefined && row !== "") {
            history.push({ pathname: "/eoffice/personnel/fileview", state: row })
        } else {
            const errorMessage = "Failed to load, kindly refresh the page !"
            callMessageOut(errorMessage);
        }

    };

    const [defaultColumnWidths, setDefaultColumnWidths] = useState([ // default column width of table 
        { columnName: 'fileName', width: 150 },
        { columnName: 'subject', width: 150 },
    ]);

    const handleChange = (event) => {
        console.log(bodyRows);

    };

    const [tableColumnExtensions] = useState([
        { columnName: 'subject', align: 'left', width: '40%', wordWrapEnabled: true },
    ])

    const { blnValuePF } = props.subscribeApi; // redux trigger that helps in refreshing table 
    useEffect(() => loadPFTableData(), [blnValuePF, currentPage]);
    
    const loadPFTableData = () => {
        setRowData([]);
        props.loadPFData(username, role,pageSize, currentPage).then((resp) => {
            let tmpArr = [];
            try {
                if (resp) { // condition to check if response then perform further 
                    if (resp.data !== undefined) {
                        tmpArr = resp.data;
                        setRowData(tmpArr);
                        setTotalCount(resp.length);
                    }
                    else {
                        const errorMessage = resp.status + " : " + resp.error + " AT " + resp.path
                        callMessageOut(errorMessage);
                    }
                    props.changingTableState(false, 'CHANGE_PA_FILE'); // setting trigger to false as table got updated
                    props.setRefresh1(false);
                }
            }
            catch (e) {
                callMessageOut(e.message)
            }
        }).catch(error => {
            console.log(error);
        });
    }
    const callMessageOut = (message) => {
        dispatch(setSnackbar(true, "error", message));
    }

    const CustomToolbarMarkup = () => ( // Custom table header
        <Plugin name="customToolbarMarkup">
            <Template name="toolbarContent">
                <div style={{ marginLeft: '25%', alignSelf: 'center' }}><Typography variant='button' align='center' color='primary'>My Personal Files</Typography></div>
            </Template>
        </Plugin>
    );

    return (
        <div>
            <Paper elevation={3} style={{ position: 'relative' }}>

                <DevGrid rows={rowData} columns={columns}>
                    <FilteringState />
                    <IntegratedFiltering />
                    <PagingState
                        currentPage={currentPage}
                        onCurrentPageChange={setCurrentPage}
                        pageSize={pageSize}
                    />
                    <CustomPaging
                        totalCount={totalCount}
                    />
                    <SortingState />
                    <GroupingState grouping={[]} />
                    <IntegratedGrouping />
                    <IntegratedSorting />
                    <SelectionState
                        selection={selection}
                        onSelectionChange={(event) => setSelection(event)}
                    />
                    <Table rowComponent={TableRow} columnExtensions={tableColumnExtensions} />
                    <TableColumnResizing defaultColumnWidths={defaultColumnWidths} />
                    <TableHeaderRow showSortingControls />
                    <TableFilterRow />
                    <Toolbar />
                    <GroupingPanel />
                    <CustomToolbarMarkup />
                    <PagingPanel />
                </DevGrid>
            </Paper>

        </div>
    );
};

function mapStateToProps(state) {

    return {
        props: state.props,
        subscribeApi: state.subscribeApi,
        refreshing: state.refreshing
    };
}

export default connect(mapStateToProps, { setRefresh1, loadPFData, changingTableState })(PersonalFileTable);
