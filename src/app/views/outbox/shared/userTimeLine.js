import React from 'react'
import '../../../../styles/layouts/layout1/userTimeLine.scss'

const userTimeLine = () => {
    let jQliList = $('ul.steps li');
    // jQliList.on('click', function(){
    //     jQliList.removeClass('active');
    //     $(this).addClass('active');
    // });

    return (
        <div className="wrapper">
            <ul className="steps">
                <li>
                    <a href="#">
                        Step One (make text multiline)
                    </a>
                </li>
                <li>
                    <a href="#">
                        Step Two
                    </a>
                </li>
                <li className="active">
                    <span>
                        Step Three (make text multiline)
                    </span>
                </li>
                <li>
                    <span>
                        Step Four
                    </span>
                </li>
                <li>
                    <span>
                        Step Five
                    </span>
                </li>
                <li>
                    <span>
                        Step Six
                    </span>
                </li>
                <li>
                    <span>
                        Step Seven (make text multiline)
                    </span>
                </li>
            </ul>
        </div>
    )
};
export default userTimeLine;