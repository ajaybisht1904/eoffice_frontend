import React, { useCallback, useEffect, useRef, useState } from "react";
import {
  FilteringState,
  IntegratedFiltering,
  PagingState,
  CustomPaging,
  SortingState,
  IntegratedSorting,
  SelectionState,
  IntegratedSelection,
} from "@devexpress/dx-react-grid";
import {
  Grid,
  Table,
  TableHeaderRow,
  PagingPanel,
  TableColumnResizing,
  TableFilterRow,
  TableSelection, Toolbar,
  ExportPanel,
} from "@devexpress/dx-react-grid-material-ui";
import {
  Icon,
  IconButton,
  Paper,
  Tooltip,
  withStyles,
  FormControl,
  TextField,
  InputLabel,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Avatar,
  Typography,
  Divider
} from "@material-ui/core";
import { Plugin, Template } from "@devexpress/dx-react-core";
import CloseIcon from '@material-ui/icons/Close';
import SearchIcon from '@material-ui/icons/Search';
import { connect, useDispatch } from "react-redux";
import { loadOutboxData } from "../../../camunda_redux/redux/action";
import { changingTableStateOutbox } from "../../../camunda_redux/redux/action/apiTriggers";
import { setSnackbar } from "app/camunda_redux/redux/ducks/snackbar";
import { GridExporter } from '@devexpress/dx-react-grid-export';
import saveAs from 'file-saver';
import DateRangeFilter from '../shared/DateRangeFilter';
import { addDays, subDays } from "date-fns";
import '../therme-source/material-ui/loading.css'

const OutboxTable = (props) => {
  const dispatch = useDispatch();
  const columns = [
    { name: "subject", title: "SUBJECT" },
    { name: "sentDate", title: "DATE SENT" },
    { name: "to", title: "TO" },
    { name: "fileName", title: "FILE #" },
    { name: "type", title: "TYPE" },
    { name: "status", title: "STATUS" },
    { name: "comments", title: "COMMENT" },
    { name: "custom", title: "ACTION" },
  ];
  const [date, setDate] = useState([
    {
      startDate: subDays(new Date(), 30),
      endDate: addDays(new Date(), 0),
      key: "selection"
    }
  ])
  const [open, setOpen] = useState(false);
  const [rowData, setRowData] = useState([]);
  const [pageSize] = useState(10);
  const [currentPage, setCurrentPage] = useState(0);
  const [totalCount, setTotalCount] = useState(0);
  const onSave = (workbook) => {
    workbook.xlsx.writeBuffer().then((buffer) => {
      saveAs(new Blob([buffer], { type: 'application/octet-stream' }), 'Outbox.xlsx');
    });
  };
  const handleChange = (event) => {
    setSelection(event);
    let tempArr = [];
    let finalArr = [];
    for (let i = 0; i < event.length; i++) {
      tempArr.push(event[i]);
    }
    for (let x = 0; x < tempArr.length; x++) {
      finalArr.push(rowData[tempArr[x]].fileName);
    }
    console.log(finalArr);
  };
  const role = sessionStorage.getItem('role');
  const userName = sessionStorage.getItem('username');
  const TableComponentBase = ({ classes, ...restProps }) => (
    <Table.Table
      {...restProps}
      className={classes.tableStriped}
    />
  );
  const styles = {
    customRow: {
      '&:hover': {
        backgroundColor: 'lightgray',
      }
    },
  };
  const TableComponent = withStyles(styles, { name: 'TableComponent' })(TableComponentBase);

  const { blnValueOutbox } = props.subscribeApi;
  useEffect(() => loadOutBoxTable(), [blnValueOutbox,currentPage]);

  const loadOutBoxTable = () => {
    props.blnEnableLoader(true)
    setRowData([]);
    props.loadOutboxData(role, userName,pageSize, currentPage).then(resp => {
      const tmpArr = [];
      try {
        if (resp !== null && resp !== undefined && resp.status != 500) {
          setTotalCount(resp.length);
          for (var i = 0; i < resp.Data.length; i++) {
            tmpArr.push(resp.Data[i]);
          }
          props.changingTableStateOutbox(false, 'CHANGE_OUTBOX');
          setRowData(tmpArr);
          props.blnEnableLoader(false)
        } else {
          const errorMessage = resp.status + " : " + resp.error + " AT " + resp.path
          callMessageOut(errorMessage);
        }
      }
      catch (e) {
        callMessageOut(e.message)
      }
    });
  }

  const callMessageOut = (message) => {
    props.blnEnableLoader(false)
    dispatch(setSnackbar(true, "error", message));
  }

  const [selection, setSelection] = useState([]);
  const [bodyRows, setBodyRows] = useState([]);

  const handleClick = (e, row) => {
    // setBodyRows(row);
    // history.push({ pathname: "/costa/file/send", state: row });
  };

  const [defaultColumnWidths, setDefaultColumnWidths] = useState([
    { columnName: 'subject', width: 420 },
    { columnName: 'sentDate', width: 120 },
    { columnName: 'to', width: 120 },
    { columnName: 'fileName', width: 180 },
    { columnName: 'type', width: 140 },
    { columnName: 'status', width: 100 },
    { columnName: 'comments', width: 300 },
    { columnName: 'custom', width: 0 },
  ]);

  const TableRow = ({ row, ...restProps }) => (
    <Table.Row
      {...restProps}
      onClick={(e) => handleClick(e, row)}
      style={{
        cursor: 'pointer'
      }}
    />
  );

  const Cell = ({ row, column, ...props }) => {
    if (column.name === "custom")
      return (
        <Table.Cell {...props} className="table-action-btn" >
          <IconButton aria-label="hrmConcernedAgency" size="small" onClick={() => { }} >
            <Tooltip title="User History" aria-label="UserHistory"><Icon style={{ fontSize: '25px' }}><img src={process.env.PUBLIC_URL + `/assets/icons/swap-box-line.svg`} alt="User History" /></Icon></Tooltip>
          </IconButton>
          <IconButton aria-label="hrmConcernedAgency" size="small" onClick={() => { }} >
            <Tooltip title="User History" aria-label="UserHistory"><Icon style={{ fontSize: '25px' }}><img src={process.env.PUBLIC_URL + `/assets/icons/history_black_24dp.svg`} alt="User History" /></Icon></Tooltip>
          </IconButton>
          <IconButton aria-label="hrmConcernedAgency" size="small" onClick={() => { }}    >
            <Tooltip title="User History" aria-label="UserHistory"><Icon style={{ fontSize: '25px' }}><img src={process.env.PUBLIC_URL + `/assets/icons/focus-3-line.svg`} alt="User History" /></Icon></Tooltip>
          </IconButton>
        </Table.Cell>
      );
    return <Table.Cell {...props} />;
  };
  const exporterRef = useRef(null);

  const startExport = useCallback(() => {
    exporterRef.current.exportGrid();
  }, [exporterRef]);


  const customizeHeader = (worksheet) => {
    const generalStyles = {
      font: { bold: true },
      fill: {
        type: 'pattern', pattern: 'solid', fgColor: { argb: 'D3D3D3' }, bgColor: { argb: 'D3D3D3' },
      },
      alignment: { horizontal: 'left' },
    };
    const headerStyle = {
      font: { bold: true, underline: true },
      alignment: { horizontal: 'center', vertical: 'middle' },
    };
    for (let rowIndex = 1; rowIndex < 4; rowIndex += 1) {
      worksheet.mergeCells(rowIndex, 1, rowIndex, 2);
      worksheet.mergeCells(rowIndex, 3, rowIndex, 4);
      Object.assign(worksheet.getRow(rowIndex).getCell(1), generalStyles);
      Object.assign(worksheet.getRow(rowIndex).getCell(3), generalStyles);
      Object.assign(worksheet.getRow(rowIndex).getCell(5), headerStyle);
    }
    worksheet.mergeCells('E1:F3');
    worksheet.getRow(1).height = 20;
    worksheet.getRow(1).getCell(1).font = { bold: true, size: 14 };
    worksheet.getRow(1).getCell(3).font = { bold: true, size: 14 };
    worksheet.getRow(1).getCell(5).font = { bold: true, size: 20, underline: true, color: { argb: '596db5' } };
    worksheet.getColumn(1).values = ['USER NAME :', 'DEPARTMENT :'];
    worksheet.getColumn(3).values = ['test1', '111WG.ACCTS'];
    worksheet.getColumn(5).values = ['OUTBOX'];
    worksheet.addRow({});
  };

  const HeaderDateRange = () => (
    <Plugin name="customToolbarMarkup">
      <Template name="toolbarContent">
        <div className="header-input">
          <Typography variant='button' color='primary' style={{ fontSize: "20px" }}>OutBox</Typography>
          <FormControl variant="outlined" size="small">
            <label style={{ 'marginBottom': '0px', 'marginRight': '15px' }}>Filter By Date Sent : </label>
            <TextField id="outlined-basic" variant="outlined" value={`${months[date[0].startDate.getMonth()]} ${date[0].startDate.getDate()}, ${date[0].startDate.getFullYear()} - ${months[date[0].endDate.getMonth()]} ${date[0].endDate.getDate()}, ${date[0].endDate.getFullYear()} `} onClick={handleClickOpen} />
            <IconButton style={{ 'margin': '0px 10px', 'backgroundColor': '#fff' }}>
              <Tooltip title="Search" aria-label="Search"><Icon><img src={process.env.PUBLIC_URL + `/assets/icons/search-line.svg`} alt="Read Only Mode" style={{ marginTop: '-10px' }} /></Icon></Tooltip>
            </IconButton>
          </FormControl>
        </div>
      </Template>
    </Plugin>
  )

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };


  const onDateChange = ranges => {
    // ranges ...
    // alert("changed check the console log");
    console.log(ranges);
    setDate([ranges])
  };

  const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']

  return (
    <Paper elevation={3} style={{ position: 'relative' }}>

      <Grid rows={rowData} columns={columns} className="outbox-table">
        <>
          <Dialog
            maxWidth='md'
            open={open}
            keepMounted
            onClose={handleClose}
            aria-labelledby="alert-dialog-slide-title"
            aria-describedby="alert-dialog-slide-description"
          >
            <DialogTitle id="alert-dialog-slide-title">
              <Typography variant='button' color='primary' style={{ fontSize: "20px" }}>Choose Date Range</Typography>
              <Typography variant='button' color='primary' style={{ fontSize: "20px" }}>
                <CloseIcon style={{ 'cursor': 'pointer' }} onClick={handleClose} />
              </Typography>
            </DialogTitle>
            <DialogContent>
              <DateRangeFilter onDateChange={onDateChange} date={date} />
            </DialogContent>

          </Dialog>
        </>
        <FilteringState columnExtensions={[{ columnName: 'custom', filteringEnabled: false }]} />
        <IntegratedFiltering />
        <PagingState
          currentPage={currentPage}
          onCurrentPageChange={setCurrentPage}
          pageSize={pageSize}
        />
        <CustomPaging
          totalCount={totalCount}
        />
        <SortingState columnExtensions={[{ columnName: 'custom', sortingEnabled: false }]} />
        <IntegratedSorting />
        <SelectionState
          selection={selection}
          onSelectionChange={handleChange}
        />
        <IntegratedSelection />
        <Table rowComponent={TableRow} tableComponent={TableComponent} cellComponent={Cell} />
        <TableColumnResizing columnWidths={defaultColumnWidths} onColumnWidthsChange={setDefaultColumnWidths} resizingMode="nextColumn" />
        <TableHeaderRow showSortingControls />
        <TableFilterRow />
        <PagingPanel />
        <Toolbar />
        <HeaderDateRange />
        <ExportPanel startExport={startExport} />
      </Grid>
      <GridExporter
        ref={exporterRef}
        rows={rowData}
        columns={columns}
        onSave={onSave}
        customizeHeader={customizeHeader}
        hiddenColumnNames={['custom']}
      />
    </Paper>
  );
};

function mapStateToProps(state) {

  return {
    props: state.props,
    subscribeApi: state.subscribeApi,
  };
}

export default connect(mapStateToProps, { loadOutboxData, changingTableStateOutbox })(OutboxTable);
