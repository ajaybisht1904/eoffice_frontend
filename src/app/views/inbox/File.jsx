import React, { Component, Fragment } from "react";
import {
    Grid, Button, ButtonGroup
} from "@material-ui/core";
import TableCard from "./shared/InboxPage";
import { withStyles } from "@material-ui/styles";
import {Breadcrumb} from "../../../matx";
import PdfViewer from "../../pdfViewer/pdfViewer";
import Loading from "../../../matx/components/MatxLoadable/Loading";
import {connect} from "react-redux";
import {createPANotingData, getbyfilename, getHrmListData} from "../../camunda_redux/redux/action";

class Inbox1 extends Component {
    state = {
        personalid:"",
        annotId:"",
        refreshes: false,
        refreshInboxData: false,
        isLoading: false,
        inboxId:'',
        showPdf: false,
        pdfLoads: false
    };

    componentDidMount() {

        this.setState({isLoading:true});
    }

    setReloadInboxData=()=>

        this.setState(previousState => ({
            refreshInboxData:!previousState.refreshInboxData
        }));

    personalID = (id) =>
    {
        this.setState({personalid:id});
    };

    inboxId = (id) =>
    {
        this.setState({inboxId:id});
    }
    annotId =(val) =>
    {
        this.setState({annotId:val});
    };

    ref = (val) =>
    {
        this.setState({refreshes:val});
    };

    handleShowPdf = (val) => {
        this.setState({showPdf: val})
    }

    render() {
        let {theme} = this.props;
        let {isLoading,personalid,annotId,refreshes} = this.state;

        if(isLoading !== true)
        {
            return (
                <Loading/>
            )
        }
        else {
            return (

                <div className="m-sm-30">
                    <div>
                        <Grid container justifyContent="center" spacing={2}>
                            <Grid item xs={4} >
                                <Breadcrumb
                                    routeSegments={[
                                        {name: "Inbox", path: "/inbox/file"}
                                    ]}
                                />
                            </Grid>
                            <Grid item xs={8}>
                            </Grid>
                        </Grid>
                    </div>
                    <div>
                        <Grid container spacing={2}>
                            <Grid item md={5} xs={12} className="inbox-table">
                                <TableCard pdfLoadsInbox={this.state.pdfLoads} refreshInboxData={this.state.refreshInboxData} refresh={this.ref}
                                    inboxId={this.inboxId}  personalId={this.personalID} annotationId={this.annotId} blnShowPdf={this.handleShowPdf}/>
                            </Grid>
                            <Grid item md={7} xs={12}>
                           <PdfViewer personalID={personalid} refreshView={refreshes} anottId={annotId} flag={"PA"} pdfLoads={(val) => {this.setState({pdfLoads: val})}} />
                            </Grid>
                        </Grid>
                    </div>
                </div>

            );
        }
    }
}
const mapStateToProps = (state) => ({
    props: state.props,
    loader: state.loader,

    });

export default connect(mapStateToProps,)(Inbox1);
