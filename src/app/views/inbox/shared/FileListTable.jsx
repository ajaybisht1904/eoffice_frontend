import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
    Button,
    List,
    ListItem,
    ListItemText,
    CircularProgress,
    Dialog,
    IconButton,
    DialogActions,
    DialogTitle,
    DialogContent,
    DialogContentText,
    InputBase,
    TextField, Icon, Typography
} from "@material-ui/core";
import FormControl from "@material-ui/core/FormControl";
import { Formik } from "formik";
import {
    FilteringState,
    IntegratedFiltering,
    PagingState,
    IntegratedPaging,
    SortingState,
    IntegratedSorting,
} from "@devexpress/dx-react-grid";
import {
    Grid as DevGrid,
    Table,
    TableHeaderRow,
    PagingPanel,
    TableColumnResizing,
    TableFilterRow,
} from "@devexpress/dx-react-grid-material-ui";
import Paper from "@material-ui/core/Paper/Paper";
import CloseIcon from '@material-ui/icons/Close';
import { connect, useDispatch } from "react-redux";
import { getHrmListData, createPANotingData, getbyfilename } from "../../../camunda_redux/redux/action";
import { setPassData } from "../../../camunda_redux/redux/ducks/passData";
import SkipNextIcon from '@material-ui/icons/SkipNext';
import TransitEnterexitIcon from '@material-ui/icons/TransitEnterexit';
import history from "../../../../history";
import SearchIcon from '@material-ui/icons/Search';
import Grid from "@material-ui/core/Grid";
import PdfViewer from "../../../pdfViewer/pdfViewer";
import { Breadcrumb } from "../../../../matx";
import NofFilesTable from "./NofFilesTable";
import { setSnackbar } from 'app/camunda_redux/redux/ducks/snackbar';
import '../therme-source/material-ui/loading.css'

const useStyles = makeStyles({
    table: {
        minWidth: 250,
    },
    button: {
        marginBottom: 10,
    },
    mainDiv: {
        textAlign: 'center',
    },
    formControl: {
        marginTop: 20,
        minWidth: 350,
        maxWidth: 350,
    },
    txtFieldFormControl: {
        minWidth: 500,
        maxWidth: 500,
    },
    chips: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    chip: {
        margin: 2,
    },
    list: {
        border: 'outset',
    },
    root: {
        padding: '2px 4px',
        display: 'flex',
        alignItems: 'center',

        background: 'whitesmoke',
        //marginLeft: 250,
        marginBottom: 10

    },
    input: {
        flex: 1,
    },
    iconButton: {
        padding: 10,
    },
});

const FileListTable = (props) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const columns = [
        { name: "name", title: "LIST OF FILES" }
    ];
    const nofColumns = [
        { name: "filename", title: "File Number" },
        { name: "fileno", title: "File Name" }
    ];
    const [blnDisableNext, setBlnDisableNext] = useState(true)
    const [openConfirmation, setOpenConfirmation] = useState(false)
    const getRowId = row => row.index;
    const [hrmConcernURL, setHRMConcernURL] = useState("")

    const TableRow = ({ row, ...restProps }) => (
        <Table.Row
            className="tableCustomRow"
            {...restProps}
            {... { hover: true }}
            // eslint-disable-next-line no-alert
            onClick={(e) => handleClick(e, row)}
            style={{
                cursor: 'pointer'
            }}
        />
    );


    const { fileID } = props;

    useEffect(() => {

        let dept = sessionStorage.getItem("department");
        const nofRows =
            { "fileno": '', "filename": "" }
        let formData = new FormData();
        formData.append('filename', '');
        formData.append('fileno', '');
        formData.append('pkldirectorate', dept);
        formData.append('row', 10);
        formData.append('skip', 0);

        props.getbyfilename(formData).then((res) => {
            console.log(res);
            try {
                if (res !== undefined) {
                    for (var i = 0; i < res.data.length; i++) {
                        let data1 = res.data[i];
                        nofRows['filename'] = data1['filename'];
                        rowData1.push(nofRows);

                    }

                } else {
                    const errorMessage = resp.status + " : " + resp.error + " AT " + resp.path
                    callMessageOut(errorMessage);
                }
            } catch (e) {
                callMessageOut(e.message)
            }
        });
        props.getHrmListData(fileID).then(resp => {
            const tmpArr = [];
            console.log(resp);
            try {
                if (resp !== undefined) {
                    tmpArr.push({ "index":0, "url": resp.personalData.fileURL, "name": resp.personalData.pfileName, "annotId": resp.personalData.annotationId,"flag":"PA" });

                    for (let i = 0; i < resp.annexureData.length; i++) {
                        tmpArr.push({ "index": i+1, "url": resp.annexureData[i].annexureFileURL, "name": resp.annexureData[i].originalFileName, "contentID": resp.annexureData[i].contentId, "annotId":resp.annexureData[i].annotationId,"flag":"Annexture" })
                    }
                    console.log(tmpArr)
                    if(resp.personalData !== undefined){
                        setHRMConcernURL(resp.personalData.fileURL)
                        props.blnShowPdf(true);
                    }
                }
                else {
                    const errorMessage = resp.status + " : " + resp.error + " AT " + resp.path
                    callMessageOut(errorMessage);
                }
                setRowData(tmpArr);
            }
            catch (e) {
                callMessageOut(e.message)
            }
        });
    }, [props.data]);

    
    useEffect(() => {
        if (props.pdfLoadsHRM == true && hrmConcernURL !== undefined) {

            props.blnShowPdf(true);
            dispatch(
                setPassData(hrmConcernURL)
            )
        }

    }, [props.pdfLoadsHRM])

    const callMessageOut = (message) => {
        props.blnEnableLoader(false)
        dispatch(setSnackbar(true, "error", message));
    }

    const handleClick = (e, row) => {
        props.annotID(row.annotId)
        props.contentID(row.contentID)
        row.flag =="PA" ? props.flagValue("PA") : props.flagValue("Annexture")
        dispatch(
            setPassData(row.url)
        )
    };

    const nofHandleClick = (val) => {
        setPersonName(val);
        setBlnOpenDialog(false);
        setBlnDisableNext(false)
    };

    const [personName, setPersonName] = React.useState([]);
    const [rowData, setRowData] = React.useState([]);
    const [rowData1, setRowData1] = React.useState([]);
    const [blnOpenDialog, setBlnOpenDialog] = React.useState(false);
    const [blnOpenRTA, setBlnOpenRTA] = React.useState(false);

    const handleChange = (event) => {
        setPersonName(event.target.value);
    };

    const [defaultColumnWidths] = useState([
        { columnName: 'name', width: 350 },
    ]);
    
    const handleOpenDialog = () => {
        setBlnOpenDialog(true);
    };

    const handleCloseDialog = () => {
        setBlnOpenDialog(false);
    };

    const handleOpenRTA = () => {
        setBlnOpenRTA(true);
    };

    const handleCloseRTA = () => {
        setBlnOpenRTA(false);
    };

    const handleRedirectToSplitView = () => {
        setOpenConfirmation(false)
        props.blnEnableLoader(true)
        const roleName = sessionStorage.getItem("role");
        const groupName = sessionStorage.getItem("department");
        const userName = sessionStorage.getItem("username");
        const InboxID = sessionStorage.getItem("InboxID");
        let formData = new FormData();
        formData.append('inboxId', InboxID)
        formData.append('applicationId',fileID)
        formData.append('fileNumber',personName)
        props.createPANotingData(formData,roleName, userName, groupName).then(resp => {
            try {
                if (resp !== undefined && resp !== "") {
                    if (resp.status === 'OK' && resp.status !== 500) {
                        props.blnEnableLoader(false)
                        history.push({ pathname: "/eoffice/splitView/file", state: { fileID: fileID } });
                    }else{
                        const errorMessage = resp.status + " : " + resp.error + " AT " + resp.path
                    
                    callMessageOut(errorMessage);
                    }
                } else {
                    const errorMessage = resp.status + " : " + resp.error + " AT " + resp.path
                    
                    callMessageOut(errorMessage);
                }
                console.log(resp);
            }
            catch (e) {
                callMessageOut(e.message)
            }
        })

    };

    return (
        <div className={classes.mainDiv}>

            <Grid container justifyContent="center" spacing={2}>
                <Grid item xs={2}></Grid>
                <Grid item xs={5}>
                    <Button variant="contained" color="primary" onClick={handleOpenRTA} className={classes.button} style={{ marginRight: '10px' }} endIcon={<Icon>
                        <img src={process.env.PUBLIC_URL + `/assets/icons/arrow-go-back-fill.svg`} alt="Remark & Check" style={{ marginTop: '-10px' }} />
                    </Icon>}>Return Application</Button>
                </Grid>
                <Grid item xs={5}>
                    <Button disabled={blnDisableNext} variant="contained" color="primary" onClick={() => setOpenConfirmation(true)} endIcon={<Icon>
                        <img src={process.env.PUBLIC_URL + `/assets/icons/arrow-right-fill.svg`} alt="Remark & Check" style={{ marginTop: '-10px' }} />
                    </Icon>} className={classes.button}>Create Part Case File</Button>
                </Grid>
            </Grid>
            <Paper elevation={3} style={{ position: 'relative' }}>
                <DevGrid rows={rowData} columns={columns} getRowId={getRowId}>
                    <FilteringState defaultFilters={[]} />
                    <IntegratedFiltering />
                    <PagingState
                        defaultCurrentPage={0}
                        pageSize={10}
                    />
                    <IntegratedPaging />
                    <SortingState />
                    <IntegratedSorting />
                    <Table rowComponent={TableRow} />
                    <TableColumnResizing defaultColumnWidths={defaultColumnWidths} />
                    <TableHeaderRow showSortingControls />
                    <PagingPanel />
                </DevGrid>
            </Paper>
            <Grid container justify="center" spacing={2} style={{ marginTop: '10px' }}>
                <Grid item xs={12}>
                    <Button variant="contained" color="primary" onClick={handleOpenDialog}>
                        SELECT FILE FOR NOF
                    </Button>
                </Grid>
            </Grid>

            <FormControl className={classes.formControl}>
                <List dense={true} className={personName.length > 0 ? classes.list : ""}>
                    {/*{personName.map((x) => (*/}
                    {/*    <ListItem>*/}
                    {/*        <ListItemText primary={x} />*/}
                    {/*    </ListItem>*/}
                    {/*))}*/}
                    <ListItem>
                        <ListItemText primary={personName} />
                    </ListItem>
                </List>
            </FormControl>
            <Dialog
                open={openConfirmation}
                onClose={() => setOpenConfirmation(false)}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
                style={{minWidth: '300px'}}
            >
                <DialogTitle id="alert-dialog-title">Confirmation?</DialogTitle>
                <DialogContent dividers>
                    <DialogContentText id="alert-dialog-description" style={{color: 'black'}}>
                        Are you sure to create a Part Case ?
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button variant="outlined" onClick={() => setOpenConfirmation(false)} color="primary">
                        Cancel
                    </Button>
                    <Button variant="outlined" onClick={handleRedirectToSplitView} color="primary" autoFocus>
                        Ok
                    </Button>
                </DialogActions>
            </Dialog>
            <Dialog
                open={blnOpenDialog}
                aria-labelledby="draggable-dialog-title"

            >
                <DialogTitle id="draggable-dialog-title" onClose={handleCloseDialog}>
                    Part Case File Creation
                    <IconButton aria-label="close" onClick={handleCloseDialog} color="primary" style={{ float: 'right' }}>
                        <CloseIcon />
                    </IconButton>
                </DialogTitle>
                <DialogContent dividers style={{ overflow: 'hidden' }}>
                    <NofFilesTable onSelectFileName={nofHandleClick} />
                </DialogContent>
            </Dialog>
            <Dialog
                open={blnOpenRTA}
                onClose={handleCloseRTA}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">"Return to Application ?"</DialogTitle>
                <DialogContent>
                        <FormControl className={classes.txtFieldFormControl}>
                            <TextField
                                id="txtReason"
                                label="Reason"
                                multiline
                                rows={4}
                                fullWidth
                                variant="outlined"
                            />
                        </FormControl>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleCloseRTA} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={handleCloseRTA} color="primary" autoFocus>
                        OK
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
};
function mapStateToProps(state) {

    return { props: state.props };
}

export default connect(mapStateToProps, { getbyfilename, getHrmListData, createPANotingData })(FileListTable);
