import React, { useEffect, useState } from 'react';
import {
    Button,
    FormControl,
    Grid,
    Icon,
    IconButton,
    makeStyles,
    TextField,
    Typography
} from "@material-ui/core";
import { Breadcrumb } from "../../../../matx";
import _without from "lodash/without";
import history from "../../../../history";
import Autocomplete from '@material-ui/lab/Autocomplete'
import { connect, useDispatch } from "react-redux";
import { getSection, getServiceNumber, sendFilesSection, sendFilesServiceNumber } from "../../../camunda_redux/redux/action";
import { setSnackbar } from 'app/camunda_redux/redux/ducks/snackbar';
import StartProcessPage from 'app/views/initiate/shared/startProcess/StartProcessPage';

const useStyles = makeStyles((theme) => ({
    gridAlignment: {
        textAlign: "center",
        display: "contents"
    },
    gridAlignLeft: {
        borderStyle: "double",
        borderWidth: "thick",
        textAlign: "center",
        paddingTop: "5% !important",
        boxShadow: "10px 10px 7px 3px #88888887"
    },
    gridAlignRight: {
        borderStyle: "double",
        borderWidth: "thick",
        textAlign: "center",
        paddingTop: "5% !important",
        boxShadow: "10px 10px 7px 3px #88888887"
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: "80%",
        maxWidth: "80%",
    },
    chips: {
        display: "flex",
        flexWrap: "wrap",

    },
    chip: {
        margin: 2,
    },
    noLabel: {
        marginTop: theme.spacing(3)
    },
    marginTop: {
        marginTop: '3%',
    },
    AlignItem: {
        textAlign: "center",
        marginTop: "25px",
    }
}));

const HrmSection = (props) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const [section, setSection] = useState([]);
    const [sectionList, setSectionList] = useState([]);
    const [service, setService] = useState([]);
    const [serviceList, setServiceList] = useState([]);
    const [blnShowWarning, setBlnShowWarning] = useState(false);
    const [blnDisable, setBlnDisable] = useState(true);
    const [bgColorLeft, setBgColorLeft] = useState('#d6d6d6');
    const [bgColorRight, setBgColorRight] = useState('#d6d6d6');
    const fileID = props.location.state.FileID;
    const inboxId = props.location.state.inboxId;

    useEffect(() => {

    }, []);

    const loadService = () => {
        props.getServiceNumber().then(resp => {
            let tempArr = [];
            try {
                if (resp !== undefined) {
                    for (let x = 0; x < resp.data.length; x++) {
                        tempArr.push(resp.data[x].userName)
                    }
                } else {
                    const errorMessage = resp.status + " : " + resp.error + " AT " + resp.path
                    callMessageOut(errorMessage);
                }
                setServiceList(tempArr);
            }
            catch (e) {
                callMessageOut(e.message)
            }
        })
    };

    const callMessageOut = (message) => {
        dispatch(setSnackbar(true, "error", message));
    }

    const loadSection = () => {
        props.getSection().then(resp => {
            let tempArr = [];
            try {
                if (resp !== undefined) {
                    for(let x = 0; x < resp.data.length; x++){
                        tempArr.push(resp.data[x].grpName)
                    }
                } else {
                    const errorMessage = resp.status + " : " + resp.error + " AT " + resp.path
                    callMessageOut(errorMessage);
                }
                setSectionList(tempArr);
            }
            catch (e) {
                callMessageOut(e.message)
            }
        })
    };

    const defaultProps = {
        options: sectionList,
        getOptionLabel: option => option
    };
    const defaultPropsSection = {
        options: serviceList,
        getOptionLabel: option => option
    };

    const handleRedirectSplitView = () => {
        if(fileID !== undefined && fileID !== null){
        history.push({ pathname: "/eoffice/splitView/file", state: {fileID: fileID, inboxId: inboxId} });
    }else {
        const errorMessage = "ID is undefined, please refresh page !"
        callMessageOut(errorMessage);
    }
    };

    const handleOnChange = (newValue) => {
        console.log("Handle On Change", newValue)
        setSection(newValue);
        // setBlnDisable(false)
        newValue.length > 0 ? handleClearListService(false) : handleClearListService(true)
    };
    const handleInputValueChange = (newValue) => {
        console.log("handleInputValueChange", newValue)
        loadSection();
        setServiceList([]);
        handleClearListService(true);
    };

    const handleOnChangeService = (newValue) => {
        console.log("handleOnChangeService", newValue)
        setService(newValue);
        // setBlnDisable(false)
        newValue.length > 0 ? handleClearListSection(false) : handleClearListSection(true)
    };
    const handleInputValueChangeService = (newValue) => {
        console.log("handleInputValueChangeService", newValue)
        loadService();
        setSectionList([]);
        handleClearListSection(true);
    };

    const handleClearListService = (val) => {
        console.log("handleClearListService", val)
        setBlnDisable(val)
        showWarning();
        setService([]);
    };
    const handleClearListSection = (val) => {
        console.log("handleClearListSection", val)
        setBlnDisable(val)
        showWarning();
        setSection([]);
    };

    const showWarning = () => {
        if (section.length > 0 || service.length > 0) {
            setBlnShowWarning(true);
        }
        else { setBlnShowWarning(false); }
    };

    const onFocusSection = () => {
        setBgColorRight('#d6d6d6');
        setBgColorLeft('white')
    };

    const onFocusService = () => {
        setBgColorLeft('#d6d6d6');
        setBgColorRight('white');
    };

    const handleSend = () => {
        const role = sessionStorage.getItem("role");
        const username = sessionStorage.getItem("username");
        const partcaseID = sessionStorage.getItem("partcaseID");
        const sectionData = {'groupName':section,'userName':username,'roleName':role, 'partCaseFile': partcaseID}
        const serviceNumberData = {'serviceNumber': service, 'userName':username, 'roleName':role, 'partCaseFile': partcaseID}
        if(section.length > 0){
            props.sendFilesSection(inboxId,JSON.stringify(sectionData),role,username).then(resp => {
                dispatch(setSnackbar(true, "success", "File has been forwarded successfully!"));
                history.push({ pathname: "/eoffice/inbox/file" });
            }).catch((error) => {
                console.log(error)
            });
        }
        else if(service.length > 0){
            props.sendFilesServiceNumber(inboxId,JSON.stringify(serviceNumberData),role,username).then(resp => {
                dispatch(setSnackbar(true, "success", "File has been forwarded successfully!"));
                history.push({ pathname: "/eoffice/inbox/file" });
            }).catch((error) =>{
                console.log(error)
            })
        }
    }

    return (
        <div className="m-sm-30">
            <div style={{marginTop: '10px'}}>
                <Grid container justifyContent="center" spacing={2}>
                    <Grid xs={5}>
                        <Breadcrumb
                            routeSegments={[
                                { name: "Split View", path: "/eoffice/splitView/file" },
                                { name: "HRM Section", path: "/eoffice/hrmSection/file" },
                            ]}
                        />
                    </Grid>
                    <Grid xs={7}>

                    </Grid>
                </Grid>
            </div>
            <Grid container justifyContent="center" spacing={2}>
                <Grid item xs={3}></Grid>
                <Grid item xs={6} className={classes.gridAlignment} >
                    <Grid item xs={3}>
                    </Grid>
                    <Grid item xs={3}>
                    </Grid>
                </Grid>
                <Grid item xs={3}></Grid>
            </Grid>
            <Grid container justify="center" spacing={2} className={classes.marginTop} style={{ height: '400px' }}>

                <Grid item xs={5} className={classes.gridAlignLeft} style={{ backgroundColor: bgColorLeft }}>
                    <Typography variant="button" color={"primary"} display="block" gutterBottom>
                        SECTION
                    </Typography>
                    <FormControl className={classes.formControl}>
                        <Autocomplete
                            multiple
                            {...defaultProps}
                            value={section}
                            id="tags-outlined"
                            onChange={(event, newValue) => {
                                handleOnChange(newValue);
                            }}
                            onFocus={onFocusSection}
                            onInputChange={(event, newInputValue) => {
                                if (newInputValue.length >= 3) {
                                    handleInputValueChange(newInputValue);
                                }
                            }}
                            filterSelectedOptions
                            renderInput={(params) => (
                                <TextField
                                    {...params}
                                    label="SEARCH BY SECTION"
                                    placeholder="Enter Section"
                                />
                            )}
                        />
                    </FormControl>
                </Grid>
                <Grid item xs={1} style={{ marginTop: '10%', textAlign: 'center' }}>
                    <Typography variant={"h3"}><span style={{ color: '#2a2aba' }}>OR</span></Typography>
                </Grid>
                <Grid item xs={5} className={classes.gridAlignRight} style={{ backgroundColor: bgColorRight }}>
                    <Typography variant="button" color={"primary"} display="block" gutterBottom>
                        SERVICE NUMBER
                    </Typography>
                    <FormControl className={classes.formControl}>
                        <Autocomplete
                            multiple
                            {...defaultPropsSection}
                            id="tags-outlined"
                            value={service}
                            onChange={(event, newValue) => { handleOnChangeService(newValue) }}
                            onFocus={onFocusService}
                            onInputChange={(event, newInputValue) => { if (newInputValue.length >= 3) { handleInputValueChangeService(newInputValue) } }}
                            filterSelectedOptions
                            renderInput={(params) => (
                                <TextField
                                    {...params}
                                    label="SEARCH BY SERVICE NUMBER"
                                    placeholder="Enter Service Number"
                                />
                            )}
                        />
                    </FormControl>
                </Grid>
            </Grid>
            <Grid container justify="center" alignItems="flex-end">
                <Grid item xs={8}></Grid>
                <Grid item xs={1} className={classes.AlignItem}>
                    <Button variant="contained" color="primary" disabled={blnDisable}
                        endIcon={<Icon><img src={process.env.PUBLIC_URL + `/assets/icons/checkbox-line.svg`} alt="Accept" style={{ marginTop: '-10px' }} /></Icon>}>
                        ACCEPT
                    </Button>
                </Grid>
                <Grid item xs={1} className={classes.AlignItem}>
                    <Button variant="contained" color="primary" disabled={blnDisable}
                        endIcon={<Icon><img src={process.env.PUBLIC_URL + `/assets/icons/close-circle-line.svg`} alt="Reject" style={{ marginTop: '-10px' }} /></Icon>}>
                        REJECT
                    </Button>
                </Grid>
                <Grid item xs={1} className={classes.AlignItem}>
                    <Button variant="contained" color="primary" onClick={handleSend} disabled={blnDisable}
                        endIcon={<Icon><img src={process.env.PUBLIC_URL + `/assets/icons/send_white_24dp.svg`} alt="Send" style={{ marginTop: '-10px' }} /></Icon>}>
                        SEND
                    </Button>
                </Grid>
            </Grid>
            {/*{blnShowWarning ?*/}
            {/*<Grid container justify="center">*/}
            {/*    <Grid item xs={12} className={classes.gridAlignment}>*/}
            {/*        <Typography variant="h6"><span style={{color: "red"}}>***Please Select Either Section Or Service Number***</span></Typography>*/}
            {/*    </Grid>*/}
            {/*</Grid> : null}*/}
           
        </div>
    )
};

function mapStateToProps(state) {

    return { props: state.props, };
}

export default connect(mapStateToProps, { getSection, getServiceNumber, sendFilesSection, sendFilesServiceNumber })(HrmSection);
