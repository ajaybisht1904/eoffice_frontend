import React from "react";
import {
    Grid
} from "@material-ui/core";
import PdfViewer from "../../../pdfViewer/pdfViewer";
import FileListTable from "./FileListTable";
import {Breadcrumb} from "../../../../matx";
import {connect} from "react-redux";
import { useState } from "react";
import { Loading } from "../therme-source/material-ui/loading";

const HrmConcerned = (props) => {

    let data = props.location.state;
    const [annotId, setAnnotId] = useState("")
    const [blnShowPDF, setBlnShowPDF] = useState(false)
    const [loading, setLoading] = useState(false);
    const [contentId, setContentId] = useState("");
    const [flag, setFlag] = useState("");
    const [pdfLoads, setPdfLoads] = useState(false);
    

    const handleShowPdf = (val) => {
        setBlnShowPDF(val)
    }

    const handleContentID = (val) => {
        setContentId(val)
    }
    const annotation = (val) => {
        console.log(val)
        setAnnotId(val)
    }

    return (
        <div className="m-sm-30">
            <div>
                <Grid container justifyContent="center" spacing={2}>
                    <Grid item xs={4}>
                        <Breadcrumb
                            routeSegments={[
                                {name: "Inbox", path: "/eoffice/inbox/file"},
                                {name: "HRM Concerned View", path: "/eoffice/hrmConcernedView/file"}
                            ]}
                        />
                    </Grid>
                    <Grid xs={8}>

                    </Grid>
                </Grid>
            </div>
            <Grid container spacing={2} style={{paddingTop: '0px'}}>
                <Grid item xs={8} >
                {blnShowPDF && <PdfViewer personalID={contentId} anottId={annotId} flag={flag} pdfLoads={(val) => {setPdfLoads(val)}}/>}
                </Grid>
                <Grid item xs={4} >
                    <FileListTable pdfLoadsHRM={pdfLoads} annotID={annotation} flagValue={(val) => setFlag(val)} contentID={handleContentID} fileID={data} blnShowPdf={handleShowPdf} blnEnableLoader={(val) => setLoading(val)}/>
                </Grid>
                {loading && <Loading />}
            </Grid>
        </div>
    );
};
function mapStateToProps(state) {

    return { props: state.props
    ,
    inboxData: state.inboxData};
}

export default  connect(mapStateToProps,)(HrmConcerned);
