import React, { useEffect, useState } from 'react';
import { connect, useDispatch } from "react-redux";
import { Button, Dialog, DialogContent, DialogTitle, FormControl, Grid, Icon, IconButton, InputLabel, makeStyles, MenuItem, Select, DialogContentText, DialogActions, Tooltip } from "@material-ui/core";
import HeadersAndFootersView from "../../FileApproval/documentEditor/editor";
import PdfViewer from "../../../pdfViewer/pdfViewer";
import { getPANotingData, getPAEnclosureData, loadPartCaseData, loadInboxDataSplitView } from "../../../camunda_redux/redux/action";
import { Loading } from "../therme-source/material-ui/loading";
import { Breadcrumb } from "../../../../matx";
import { setInboxDatas } from "../../../redux/actions/InboxActions";
import history from "../../../../history";
import { setPassData } from "../../../camunda_redux/redux/ducks/passData";
import { setSnackbar } from 'app/camunda_redux/redux/ducks/snackbar';
import CloseIcon from '@material-ui/icons/Close';
import InputForm from './quickSignFrom';
import '../therme-source/material-ui/loading.css'
import SplitViewPdfViewer from './pdfViewer/pdfViewer'

const useStyles = makeStyles({
    mainDiv: {
        textAlign: 'center',
    },
    formControl: {
        marginTop: 10,
        minWidth: 350,
        maxWidth: 350,
    },
    button: {
        marginTop: 12,
        marginLeft: 4,
    },
});
const SplitView = (props) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const [NOF, setNOF] = useState("");
    const [NOF1, setNOF1] = useState("");
    const [sfdtData, setSfdtData] = useState("");
    const FileID = sessionStorage.getItem("pa_id")
    // const FileID = props.location.state.fileID;
    const InboxIdFromHrmSection = sessionStorage.getItem("InboxID")
    // const InboxIdFromHrmSection = props.location.state.inboxId;
    const [blnVisible, setBlnVisible] = useState(false);
    const [fileName, setFileName] = useState("");
    const [loading, setLoading] = useState(false);
    const [rowID, setRowID] = useState("");
    const [enclosureData, setEnclosureData] = useState([]);
    const inboxId = sessionStorage.getItem("InboxID")
    const [open, setOpen] = useState(false);
    const [URL, setURL] = useState("");
    const [pdfLoads, setpdfLoads] = useState(false);
    const [blnHideSyncfusion, setBlnHideSyncfusion] = useState(false);
    const [fileChange, setFileChange] = useState(false)
    const [pdfWidth, setpdfWidth] = useState(6)
    const [notingURL, setNotingURL] = useState("")
    
    const [pdfViewerButtons, setPdfViewerButtons] = useState([
        {
            'btnName': 'A',
            'btnId': 0,
            'backgroundColor': 'grey',
            'fileurl': ''
        },
        {
            'btnName': 'B',
            'btnId': 1,
            'backgroundColor': 'grey',
            'fileurl': ''
        },
        {
            'btnName': 'C',
            'btnId': 2,
            'backgroundColor': 'grey',
            'fileurl': ''
        },
        {
            'btnName': 'D',
            'btnId': 3,
            'backgroundColor': 'grey',
            'fileurl': ''
        },
        {
            'btnName': 'E',
            'btnId': 4,
            'backgroundColor': 'grey',
            'fileurl': ''
        }
    ])

    useEffect(() => {
        setLoading(true);
        const InboxID = (inboxId !== undefined) ? inboxId : InboxIdFromHrmSection;
        console.log(sessionStorage.getItem("pa_id"));
        const inboxId = sessionStorage.getItem("InboxID");
        props.loadInboxDataSplitView(inboxId).then(resp => {

            if (resp.Data.partCaseFile) {
                sessionStorage.setItem("partcaseID", resp.Data.partCaseFile)
                let formData = new FormData();
                formData.append("id", resp.Data.partCaseFile);
                props.loadPartCaseData(formData).then(resp => {
                    console.log(resp)
                    if (resp != undefined) {
                        console.log(resp)
                        let enclouserTmpArr = resp.enclosure_file
                        let notngTmpArr = resp.noting_file
                        console.log(notngTmpArr)
                        setEnclosureData(enclouserTmpArr)
                        setRowID(notngTmpArr.fileId)
                        setSfdtData(notngTmpArr.fileUrl)
                        setFileName(notngTmpArr.fileName)
                        setNOF(notngTmpArr.fileName)
                        setBlnVisible(true);
                        setLoading(false);
                    }

                });
            }
        });
    }, [])

    const callMessageOut = (message) => {
        dispatch(setSnackbar(true, "error", message));
    }

    const handleChange = (event) => {
        setNOF1("")
        setNOF(event.target.value);
         let url = event.target.value.fileUrl
        dispatch(
            setPassData(url)
        )
    };
    const handleChange1 = (event) => {
        console.log(event.target.value)
        setNOF1(event.target.value.fileUrl)
        let url = event.target.value.fileUrl
        let ID = event.target.value.fileId
        setFileChange(true)
        setURL(url)
        setSfdtData(url)
        setRowID(ID)
        // setFileUrl(url);
        dispatch( 
            setPassData(url)
        )
    };

    const handleRedirectToHrm = (row) => {
        {
            props.setInboxDatas(row);
            history.push({ pathname: "/eoffice/hrmSection/file", state: { FileID, inboxId } });
        }
    };

    const pdfCustomButton = (e) => {
        let elementName = e.target.parentElement.getAttribute('buttonName');
        let fileurl = e.target.parentElement.getAttribute('fileurl');
        const tempColour = ['orange', 'green', 'purple', 'blue', 'mediumvioletred']
        var urlExist = true;
        var resUrl = '';
        for (let x = 0; x < pdfViewerButtons.length; x++) {
            if (pdfViewerButtons[x].fileurl === URL) {
                urlExist = false
            }
            if (fileurl) {
                resUrl = fileurl
            }
        }
        if (resUrl) {
            dispatch(
                setPassData(resUrl)
            )
        } else {
            let updatedElement = pdfViewerButtons.map((item) =>
                item.btnId == elementName && fileChange && urlExist ? {
                    ...item,
                    'backgroundColor': tempColour[item.btnId],
                    'fileurl': URL
                } : item
            )
            setFileChange(false)
            setPdfViewerButtons(updatedElement)
        }


    }
    const handleSignedCompleted = (val) => {
        setpdfWidth(6)
        setBlnHideSyncfusion(val)
        setOpen(false)
    }

    const handleReturnedURL = (url) => {
        console.log("URL : ", url)
        setNotingURL(url)
    }

    return (
        <div className="m-sm-30" style={{ "marginRight": "10px" }}>
            <div>
                <Grid container justifyContent="center" spacing={2}>
                    <Grid item xs={4}>
                        <Breadcrumb
                            routeSegments={[
                                {name: "Inbox", path: "/eoffice/inbox/file"},
                                { name: "Split View", path: "/costa/splitView/file" }
                            ]}
                        />
                    </Grid>
                    <Grid item xs={8}>

                    </Grid>
                </Grid>
            </div>
            <Grid container justifyContent="center" spacing={1}>
                <Grid item xs={6}>
                    {blnVisible ?
                        <FormControl variant="outlined" size="small" className={classes.formControl}>
                            <InputLabel id="demo-simple-select-outlined-label">NOTE ON FILE</InputLabel>
                            <Select
                                labelId="demo-simple-select-outlined-label"
                                id="demo-simple-select-outlined"
                                value={NOF}
                                onChange={handleChange}
                                label="NoteOnFile"
                            >
                                <MenuItem value={fileName}>
                                    <em>{fileName}</em>
                                </MenuItem>

                            </Select>
                        </FormControl>
                        : null}
                    <Button style={{ "marginBottom": "15px" }} variant="contained" color="primary" className={classes.button} onClick={() => setOpen(true)}>
                        REMARKS AND SIGN
                        <Icon>
                            <img src={process.env.PUBLIC_URL + `/assets/icons/fact_check_white_24dp.svg`} alt="Remark & Check" style={{ marginTop: '-10px' }} />
                        </Icon>
                    </Button>
                   
                </Grid>
                <Grid item xs={6}>
                    <FormControl variant="outlined" size="small" className={classes.formControl}>
                        <InputLabel id="demo-simple-select-outlined-label">ENCLOUSER</InputLabel>
                        <Select
                            labelId="demo-simple-select-outlined-label"
                            id="demo-simple-select-outlined1"
                            value={NOF1.fileUrl}
                            onChange={handleChange1}
                            label="NoteOnFile"
                        >
                            {enclosureData.map((item, index) =>
                                <MenuItem key={index} value={item}>{item.fileName}</MenuItem>
                            )}
                        </Select>
                    </FormControl>
                    <Button variant="contained" color="primary" className={classes.button} onClick={() => setOpen(true)}>
                        REMARKS AND SIGN
                        <Icon>
                            <img src={process.env.PUBLIC_URL + `/assets/icons/fact_check_white_24dp.svg`} alt="Remark & Check" style={{ marginTop: '-10px' }} />
                        </Icon>
                    </Button>
                    <Button variant="contained" color="primary" className={classes.button} onClick={handleRedirectToHrm}>
                        SEND TO
                        <Icon>
                            <img src={process.env.PUBLIC_URL + `/assets/icons/arrow-right-fill.svg`} alt="Next" style={{ marginTop: '-10px' }} />
                        </Icon>
                    </Button>
                </Grid>
                {!blnHideSyncfusion ? <Grid item xs={6}>
                {blnVisible ?
                        <HeadersAndFootersView fileId={rowID} fileUrl1={sfdtData} blnIsPartCase={true} />
                        : loading && <Loading />
                    }
                </Grid> :
                <Grid item xs={6} style={{ "flexWrap": "nowrap" }}>
                    <SplitViewPdfViewer fileUrl={notingURL} pdfLoads={(val) => { setpdfLoads(val) }} />
                    </Grid>}
                <Grid item xs={pdfWidth}>
                <Grid container style={{ "flexWrap": "nowrap" }}>
                        <Grid item style={{ 'width': '95%' }}>
                            <PdfViewer personalID={""} fileUrl={""} pdfLoads={(val) => { setpdfLoads(val) }} />
                            {/* <PdfViewer personalID={""} fileUrl={""} /> */}
                        </Grid>
                        <Grid item>
                            <div className="split-custom-btn-wrapper">
                                {
                                    pdfViewerButtons.map((item) => {
                                        return (
                                            <Button key={item.btnId} size={'small'} fileurl={item.fileurl} buttonname={item.btnId} style={{ backgroundColor: item.backgroundColor }} onClick={e => pdfCustomButton(e)} className="split-btn-custom" variant="contained" color="primary" href="#contained-buttons">
                                                {item.btnName}
                                            </Button>
                                        )
                                    })
                                }

                            </div>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
            <Dialog
                open={open}
                aria-labelledby="draggable-dialog-title"
            >
                <DialogTitle id="draggable-dialog-title" onClose={() => setOpen(false)}>
                    Remark & Sign
                    <IconButton aria-label="close" onClick={() => setOpen(false)} color="primary" style={{ float: 'right' }}>
                        <CloseIcon />
                    </IconButton>
                </DialogTitle>
                <DialogContent dividers>
                    <InputForm callBackURL={handleReturnedURL} isSignedCompleted={handleSignedCompleted} fileId={rowID} SignURL={sfdtData} />
                </DialogContent>
            </Dialog>
        </div>
    )
};

const mapStateToProps = (state) => ({
    props: state.props,

    inboxer: state.inboxer
});
export default connect(mapStateToProps, { setInboxDatas, loadInboxDataSplitView, loadPartCaseData, getPANotingData, getPAEnclosureData })(SplitView);
