import React, {useEffect, useRef, useState} from 'react';
import WebViewer from '@pdftron/webviewer';
import './App.css';
import {connect} from "react-redux";

const SplitViewPdfViewer = (props) => {
    const viewer = useRef(null);
    const {flag} = props;
    let annotatId = props.anottId;
    const [instance,setInstance]=useState(null);
    let [loading,setLoading] = useState(true);
    
    useEffect( () => {
        if(instance!=null){
            instance.loadDocument(props.fileUrl, {});
            setInstance(instance);
        }
        else{


        WebViewer(
            {
                path: `${process.env.PUBLIC_URL+'/webviewer/lib'}`,
                initialDoc: 'http://11.0.0.118:9000/template/sample.pdf'+'?token='+sessionStorage.getItem('jwt_token'),
                fullAPI: true,
                enableRedaction: true,
                backendType: 'ems',
            },
            viewer.current,
        ).then((instance) => {
            setInstance(instance);

            const { annotManager ,docViewer, Tools } = instance;
            var FitMode = instance.FitMode;
            let data = sessionStorage.getItem("userInfo");
            let userInfo = JSON.parse(data);

            annotManager.setCurrentUser(userInfo.name);
            annotManager.setIsAdminUser(true);
            docViewer.on('documentLoaded',function(){
                instance.setFitMode(FitMode.FitWidth)
            });
            const fullScreen = {
                type: 'actionButton',
                img: '<svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M4 8v-2a2 2 0 0 1 2 -2h2" /><path d="M4 16v2a2 2 0 0 0 2 2h2" /><path d="M16 4h2a2 2 0 0 1 2 2v2" /><path d="M16 20h2a2 2 0 0 0 2 -2v-2" /></svg>',
                title:'FullScreen',
                onClick: () => {
                    instance.toggleFullScreen(true);
                },
                dataElement: 'fullscreen',
            };


            // Add a new button that alerts "Hello world!" when clicked
            // instance.setHeaderItems((header) => {
            //     header.push(fullScreen)
            // })
            props.pdfLoads(true)
            // const displaymode=new DisplayMode(docViewer, mode, false);
            // instance.setDisplayMode((doc)=>{

            // })

        });}
    }, [props.fileUrl]);


    // Make a GET request to get XFDF string
    var loadxfdfStrings = function(documentId) {
        return props.getAnnotation(documentId)
    };

    return (
        <div  className="App">
            <div id = "pdfV" className="webviewer" ref={viewer} style={{height: "80vh"}}></div>
        </div>
    );
};

function mapStateToProps(state) {

    return { 
        props: state.props };
}

    export default connect(mapStateToProps,{})(SplitViewPdfViewer);
