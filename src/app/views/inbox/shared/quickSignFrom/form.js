import React from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import {FormControlLabel, FormLabel, ListItemIcon, makeStyles, MenuItem, Radio, RadioGroup} from "@material-ui/core";
import CommentIcon from '@material-ui/icons/Comment';
import ColorLensIcon from '@material-ui/icons/ColorLens';
import StopIcon from '@material-ui/icons/Stop';
import CheckIcon from '@material-ui/icons/Check';
import { Field } from "formik";

const useStyles = makeStyles((theme) => ({
	MenuProperty : {
		position: 'relative',
	},
	ButtonMarginLeft: {
		margin : '15px 0px 20px 5px'
	}
}));

const options = ["Red", "Green", "Blue", "Black"];

const renderOptions = (options) => {
  return options.map((option) => (
    <FormControlLabel
      key={option}
      value={option}
      control={<Radio color='primary' />}
      label={option}
    />
  ));
};

const FormikRadioGroup = ({ field, name, options, children, ...props }) => {
	const fieldName = name || field.name;
  
	return (
	  <div style={{marginTop: '20px'}}>
		<FormLabel component="legend" style={{ display: "flex", fontWeight: '400', color: 'black' }}>
		  Color
		</FormLabel>
		<RadioGroup
		  {...field}
		  {...props}
		  name={fieldName}
		  style={{ position: "relative", display: "table-cell" }}
		>
		  {options ? renderOptions(options) : children}
		</RadioGroup>
	  </div>
	);
  };

  
export const Form = props => {
	const classes = useStyles(); //tag, signTitle, username, dep_desc, color
	const {
		values: { comments, pencilColorCode },
		errors,
		touched,
		handleSubmit,
		handleChange,
		isValid,
		setFieldTouched
	} = props;

	const change = (name, e) => {
		e.persist();
		handleChange(e);
		setFieldTouched(name, true, false);
	};
	
	return (
		<form onSubmit={handleSubmit} style={{"margin": "10px",zIndex:35001}}>

			<TextField
				name="comments"
				helperText={touched.comments ? errors.comments : ""}
				error={Boolean(errors.comments)}
				label="Comment"
				value={comments || ''}
				onChange={handleChange}
				fullWidth
				multiline
				rows={4}
				variant='outlined'
				InputProps={{
					endAdornment: (
						<InputAdornment position="start">
							<CommentIcon />
						</InputAdornment>
					)
				}}
			/>
			<div style={{fontSize: 'small', color: 'red', textAlign: 'end'}}>{Boolean(errors.comments) ? errors.comments : ""}</div>
			<Field
        name="pencilColorCode"
        value={pencilColorCode}
        options={options}
        component={FormikRadioGroup}
        helperText={touched.pencilColorCode ? errors.pencilColorCode : ""}
        error={Boolean(errors.pencilColorCode)}
      />
			
			<div style={{fontSize: 'small', color: 'red', textAlign: 'end'}}>{Boolean(errors.pencilColorCode) ? errors.pencilColorCode : ""}</div>
			<div style={{textAlign: 'end', marginLeft: 'auto', marginRight: 'auto'}}>
			<Button
				type="submit"
				variant="outlined"
				color="primary"
				disabled={!isValid}
				endIcon={<CheckIcon />}
				className={classes.ButtonMarginLeft}
			>
				Sign
			</Button>
			</div>
		</form>
	);
};
