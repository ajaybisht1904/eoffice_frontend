import { render } from 'react-dom';
import './index.css';
import * as React from 'react';
import { SampleBase } from './sample-base';

import { DocumentEditorContainerComponent, Toolbar, Print  } from '@syncfusion/ej2-react-documenteditor';
import { TitleBar } from './title-bar';
import {PropTypes} from "prop-types";
import {connect} from "react-redux";
import {saveDocument} from "../../../camunda_redux/redux/action";
import {Fab} from '@material-ui/core';
import SendIcon from '@material-ui/icons/Send';
import {setSnackbar} from "../../../camunda_redux/redux/ducks/snackbar";

DocumentEditorContainerComponent.Inject(Toolbar, Print);
// tslint:disable:max-line-length

class HeadersAndFootersView extends SampleBase {

    constructor(props) {
        super(...arguments);
        this.state = {
            newToolBar: [],
            documenteditor: null,
            blnOpenQuickSin: false,
            hostUrl:'',
            willAutoSave:true,
           // contentChanged : false
        };
       this.contentChanged=false
    }
timeout=0;

    onLoadDefault =  async () => {
        console.log("Loading");

         let response = await fetch(this.props.fileUrl1);
        let data = await response.blob();

        let file = new File([data], "test.docx");
        this.loadFile(file);

         let saveButton = {
             prefixIcon: "e-save-icon",
             tooltipText: "Save",
             text: "Save",
             id: "save",
             type: "Button",

         };
         let sendButton = {
             prefixIcon: "e-send-icon",
             tooltipText: "Send",
             text: "Send",
             id: "send",
             type: "Button",

         };
         let downloadButton = {
             prefixIcon: "e-download-icon",
             tooltipText: "Download",
             text: "Download",
             id: "download",
             type: "Button",

         };
         let printButton = {
             prefixIcon: "e-print-icon",
             tooltipText: "Print",
             text: "Print",
             id: "print",
             type: "Button",

         };

         this.props.blnShowQuickSign ?
         this.setState({newToolBar:[saveButton,sendButton,'Open','Undo','Redo','Separator','Image','Table','Hyperlink','Bookmark','Comments','TrackChanges','TableOfContents','Separator','Header','Footer','PageSetup','PageNumber','Break','Separator','Find','Separator','LocalClipboard','RestrictEditing',downloadButton,printButton]})
         : this.setState({newToolBar:[saveButton,'Open','Undo','Redo','Separator','Image','Table','Hyperlink','Bookmark','Comments','TrackChanges','TableOfContents','Separator','Header','Footer','PageSetup','PageNumber','Break','Separator','Find','Separator','LocalClipboard','RestrictEditing',downloadButton,printButton]})
          
     };
    loadFile(file) {
        let ajax = new XMLHttpRequest();
        ajax.open('POST', process.env.REACT_SYNCFUSION_URL, true);
        ajax.onreadystatechange = () => {
            if (ajax.readyState === 4) {
                if (ajax.status === 200 || ajax.status === 304) {
                    this.container.documentEditor.open(ajax.responseText);
                }
            }
        };
        let formData = new FormData();
        formData.append('files', file);
        ajax.send(formData);
    }

    rendereComplete() {
        let data = sessionStorage.getItem("username");
        this.container.serviceUrl = process.env.REACT_SYNCFUSION_SERVICE_URL;
        this.container.documentEditor.pageOutline = '#E0E0E0';
        this.container.documentEditor.acceptTab = true;
        this.container.documentEditor.resize();
        this.container.documentEditor.currentUser = data;
        this.titleBar = new TitleBar(document.getElementById('documenteditor_titlebar'), container, true);
        this.onLoadDefault();
       // if(this.state.willAutoSave){
       this.timeout= setInterval(() => {
            if (this.contentChanged ) {
                //You can save the document as below
               // alert("saving")
                this.container.documentEditor.saveAsBlob("Docx").then(blob => {
                    let exportedDocument = blob;
                    var file = new File([blob], "SampleFile.docx");
                    let formData=  new FormData();
                    formData.append('file', file);
                    console.log(exportedDocument);
                    var reader = new FileReader();
                    reader.onload = function() {
                        console.log(reader.result);
                    };
                    reader.readAsText(blob);
                    var fileID = this.props.fileId;
                    console.log(fileID);
                    this.props.saveDocument(fileID,formData,this.role).then(resp => {
                        console.log(resp);
                        
                    });
                });
                this.contentChanged = false;
            }
        }, 2000);
    //}
        this.container.contentChange = () => {
            //this.setState({contentChange:true})
            this.contentChanged=true
        };

    }
    update(value){
        console.log(value);
    }

    send(props){
        const { sendToogle } = props;
        sendToogle(true);
    }
    role = sessionStorage.getItem("role");

      save(props) {
        this.container.documentEditor.saveAsBlob("Docx").then(blob => {
            let exportedDocument = blob;
            var file = new File([blob], "SampleFile.docx");
            let formData=  new FormData();
            formData.append('file', file);
            formData.append('isPartCase', props.blnIsPartCase)
            var reader = new FileReader();
            reader.onload = function() {
                console.log(reader.result);
            };
            reader.readAsText(blob);
            var fileID = this.props.fileId;
            console.log(fileID);
             this.props.saveDocument(fileID,formData,this.role, props.blnIsPartCase).then(resp => {
                console.log(resp);
                this.props.setSnackbar(
                    true,
                    "success",
                    "Document Saved successfully!"
                );
            });
        });
    };

    print = value => {
       this.titleBar.onPrint();
    };

    

      async createFile(){
          const { fileUrl1 } = this.props;
        console.log(fileUrl1);

        let response = await fetch(fileUrl1);

        let data = await response.blob();
        let file = new File([data], "test.docx");
        console.log(file[0]);
        this.loadFile(file[0]);
      };
     
    //   componentWillUnmount() {
    //      this.save(this.props);
    //   }


    render() {

        return (<div className='control-pane'>
            <div className='control-section'>

                <div id="documenteditor_container_body">

                    <DocumentEditorContainerComponent id="container" ref={(scope) => { this.container = scope; }} style={{ 'display': 'block' }} height={'670px'}
                    enableToolbar={true}
                    showPropertiesPane={false}
                    // enableEditorHistory={true}
                    // enableAllModules={true}
                    toolbarItems={this.state.newToolBar}
                    toolbarClick={(args) => {
                    switch(args.item.id){
                        case 'save':
                            this.save(this.props);
                            break;
                        case 'send':
                            this.send(this.props);
                            break;
                        case 'print':
                            this.print;
                            break;
                        // case 'quickSign':
                        //     this.openQuickSignDiv(this.props);
                        //     break;
                    }}} />
                    
                </div>
            </div>
            <script>{window.onbeforeunload = function () {
                return 'Want to save your changes?';
            }}
            </script>

        </div>
    );}
}
const mapStateToProps = state => ({
    saveDocument: PropTypes.func.isRequired,
});
export default
    connect(
        mapStateToProps,
        { saveDocument,setSnackbar }
    )(HeadersAndFootersView);
