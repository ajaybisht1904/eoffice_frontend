import React, { useState, useEffect } from "react";
import {
  Paper, Typography
} from "@material-ui/core";
import {
  Grid as DevGrid, GroupingPanel, PagingPanel,
  Table,
  TableColumnResizing,
  TableHeaderRow,
  Toolbar
} from "@devexpress/dx-react-grid-material-ui";
import {
  GroupingState,
  IntegratedFiltering, IntegratedGrouping,
  IntegratedPaging, IntegratedSorting,
  PagingState, SelectionState,
  SortingState, CustomPaging
} from "@devexpress/dx-react-grid";
import { Plugin, Template } from "@devexpress/dx-react-core";
import { loadPATableData } from '../../../camunda_redux/redux/action'
import { makeStyles } from "@material-ui/core/styles";
import { connect, useDispatch } from "react-redux";
import { setSnackbar } from '../../../camunda_redux/redux/ducks/snackbar'


const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  input: {
    display: 'none',
  },
  divZIndex: {
    zIndex: '0',

  },
  gridHeader: {
    fontSize: '22px',
    textAlign: 'center',
    margin: '3% 0',
  },
}));

const TableCard = (props) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const columns = [
    { name: "subject", title: "SUBJECT" },
    { name: "createdOn", title: "DATE APPLIED" },
    { name: "status", title: "STATUS" },
    { name: "pfileName", title: "APPLICATION #" },
  ];
  const [rowData, setRowData] = useState([]);
  let username = sessionStorage.getItem("username");
  let role = sessionStorage.getItem("role");
  let dept = sessionStorage.getItem("department");
  const [pageSize] = useState(10);
  const [currentPage, setCurrentPage] = useState(0);
  const [totalCount, setTotalCount] = useState(0);


  const TableRow = ({ row, ...restProps }) => ( // Custom table row template
    <Table.Row
      {...restProps}
      {... { hover: true }}
      style={{
        cursor: 'pointer',
        '&:hover': {
          backgroundColor: 'lightgray',
        }
      }}
    />
  );

  useEffect(() => loadPATableData(), [currentPage]);

  const loadPATableData = () => {
    setRowData([]);
    props.loadPATableData(username, role, pageSize, currentPage).then((resp) => {
      try {
        let tmpArr = []
        let PACount = 0
        if (resp) {
          console.log(resp)
          console.log(JSON.stringify(resp))
          if (resp.data !== undefined) {
            setTotalCount(resp.length);
            tmpArr = resp.data
            setRowData(tmpArr)
            PACount = resp.data.length
            props.totalCountPA(PACount)
          }
          else {
            const errorMessage = resp.status + " : " + resp.error + " AT " + resp.path
            callMessageOut(errorMessage)
          }
        }
      }
      catch (e) {
        callMessageOut(e.message)
      }
    }).catch(error => {
      console.log(error)
    });
  }

  const callMessageOut = (message) => {
    dispatch(setSnackbar(true, "error", message));
}

  const CustomToolbarMarkup = () => ( // Custom table header of table 
    <Plugin name="customToolbarMarkup">
      <Template name="toolbarContent">
        <div style={{ marginLeft: '40%', alignSelf: 'center' }}><Typography variant='button' align='center' color='primary'>My Personal Applications</Typography></div>
      </Template>
    </Plugin>
  );

  const [tableColumnExtensions] = useState([
    { columnName: 'subject', align: 'left', width: '50%' },
  ])

  return (
    <Paper elevation={3} style={{ position: 'relative' }}>
      <DevGrid rows={rowData} columns={columns}>
        <IntegratedFiltering />
        <PagingState
                    currentPage={currentPage}
                    onCurrentPageChange={setCurrentPage}
                    pageSize={pageSize}
                />
                <CustomPaging
                    totalCount={totalCount}
                />
        <SortingState />
        <GroupingState grouping={[]} />
        <IntegratedGrouping />
        <IntegratedSorting />
        <SelectionState
        />
        <Table rowComponent={TableRow} columnExtensions={tableColumnExtensions} />
        <TableHeaderRow showSortingControls />
        <Toolbar />
        <GroupingPanel />
        <CustomToolbarMarkup />
        <PagingPanel />
      </DevGrid>
    </Paper>
  )
};
function mapStateToProps(state) {

  return {
    props: state.props
  };
}

export default connect(mapStateToProps, { loadPATableData })(TableCard);
