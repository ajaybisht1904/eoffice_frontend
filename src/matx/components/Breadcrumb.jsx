import React, { Fragment } from "react";
import {Icon, Tooltip} from "@material-ui/core";
import { NavLink } from "react-router-dom";

const Breadcrumb = ({ routeSegments }) => {
  return (
    <div className="flex flex-middle position-relative">
      {/* {routeSegments ? (
        <Fragment>
          <h4 style={{fontSize:'12px'}} className="m-0 pb-2 font-size-16 capitalize text-middle">
            {routeSegments[routeSegments.length - 1]["name"]}
          </h4>
          <h4 className="m-0 pb-2 ml-8 text-hint">|</h4>
        </Fragment>
      ) : null} */}
    
      
      <NavLink to="/eoffice/dashboard/analytics">
        <Icon style={{fontSize:'15px'}} className="text-middle ml-8 mb-1" color="primary">
            <Tooltip title="Home" aria-label="Home"><Icon style={{fontSize : '20px', marginTop: '-7px', height: '25px'}}><img src={process.env.PUBLIC_URL+`/assets/icons/home_black_24dp.svg`} alt="Home" /></Icon></Tooltip>
        </Icon>
      </NavLink>
      {routeSegments
        ? routeSegments.map((route, index) => (
            <Fragment key={index}>
              <Icon className="text-hint">navigate_next</Icon>
              {index !== routeSegments.length - 1 ? (
                <NavLink to={route.path}>
                  <span style={{fontSize:'12px'}} className="capitalize text-muted">{route.name}</span>
                </NavLink>
              ) : (
                <span style={{fontSize:'12px'}} className="capitalize text-muted">{route.name}</span>
              )}
            </Fragment>
          ))
        : null}
    </div>
  );
};

export default Breadcrumb;
