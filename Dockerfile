FROM nginx:1.20.1-alpine
COPY default.conf /etc/nginx/conf.d/
COPY nginx.crt /etc/ssl/
COPY nginx.key /etc/ssl
RUN mkdir /usr/share/nginx/html/eoffice
COPY /build /usr/share/nginx/html/eoffice
COPY /build/index.html /usr/share/nginx/html


